<?php
use App\Models\Member;
use Faker\Generator as Faker;

$factory->define(Member::class, function (Faker $faker) {
    return [
        'name'  => $faker->name,
        'phone' => $faker->unique()->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'gender'=> $faker->randomElement(['Male', 'Female']),
        'address' => $faker->address
    ];
});
