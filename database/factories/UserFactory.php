<?php

use Illuminate\Support\Str;
use Faker\Generator as Faker;
use App\Models\User;

$factory->define(User::class, function (Faker $faker) {
    return [
        'name'     => $faker->name,
        'username' => $faker->words(6,12),
        'email'    => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'phone'    => $faker->phoneNumber,
        'address'  => $faker->address,
        'remember_token' => Str::random(10),
    ];
});
