<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReimbursementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reimbursements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type', ['medicine', 'equipment']);
            $table->integer('medicine_id')->unsigned()->index()->nullable();
            $table->integer('equipment_id')->unsigned()->index()->nullable();
            $table->integer('disburse')->nullable(); // ยืม
            $table->integer('takeback')->nullable(); // คืน
            $table->integer('balance')->nullable();  // คงเหลือ
            $table->enum('status', ['disburse', 'takeback']); // สถานะ
            $table->datetime('datetime')->nullable();
            $table->timestamps();
            $table->foreign('medicine_id')->references('id')->on('medicines')->onDelete('cascade');
            $table->foreign('equipment_id')->references('id')->on('equipment')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reimbursements');
    }
}
