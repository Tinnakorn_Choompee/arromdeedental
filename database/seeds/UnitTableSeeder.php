<?php

use Illuminate\Database\Seeder;
use App\Models\Unit;

class UnitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Unit::create(['name' => "เม็ด", 'type' => 'medicine']);
        Unit::create(['name' => "ขวด", 'type' => 'medicine']);

        Unit::create(['name' => "อัน",  'type' => 'equipment']);
        Unit::create(['name' => "ชุด",  'type' => 'equipment']);
    }
}
