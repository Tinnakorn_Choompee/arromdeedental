<?php

use Illuminate\Database\Seeder;
use App\Models\Medicine;

class MedicinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Medicine::create([
            'code'       => "MC001",
            'name'       => 'Paracetamol tablet 500mg',
            'properties' => 'ยาน้ำบรรเทาอาการปวด',
            'amount'     => 100,
            'type'       => 'เม็ด'
        ]);

        Medicine::create([
            'code'       => "MC002",
            'name'       => 'Paracetamol syrup 120mg/5ml',
            'properties' => 'ยาน้ำบรรเทาอาการปวด',
            'amount'     => 100,
            'type'       => 'ขวด'
        ]);

        Medicine::create([
            'code'       => "MC003",
            'name'       => 'Ibuprofen(400)+Paracetamol(325) tablet',
            'properties' => 'ยาน้ำบรรเทาอาการปวด',
            'amount'     => 100,
            'type'       => 'เม็ด'
        ]);

        Medicine::create([
            'code'       => "MC004",
            'name'       => 'Amoxicillin tablet 500mg',
            'properties' => 'ยาแก้อักเสบ',
            'amount'     => 100,
            'type'       => 'เม็ด'
        ]);

        Medicine::create([
            'code'       => "MC005",
            'name'       => 'Amoxicillin syrup 125mg/5ml',
            'properties' => 'ยาแก้อักเสบ',
            'amount'     => 100,
            'type'       => 'ขวด'
        ]);

        Medicine::create([
            'code'       => "MC006",
            'name'       => 'Amoxicillin syrup 250mg/5ml',
            'properties' => 'ยาแก้อักเสบ',
            'amount'     => 100,
            'type'       => 'ขวด'
        ]);

        Medicine::create([
            'code'       => "MC007",
            'name'       => 'Clindamycin tablet 300mg',
            'properties' => 'ยาแก้อักเสบ',
            'amount'     => 100,
            'type'       => 'เม็ด'
        ]);

        Medicine::create([
            'code'       => "MC008",
            'name'       => 'Metronidazole tablet 400mg',
            'properties' => 'ยาแก้อักเสบ',
            'amount'     => 100,
            'type'       => 'เม็ด'
        ]);

        Medicine::create([
            'code'       => "MC009",
            'name'       => '0.1% Trimcinolone acetonide',
            'properties' => 'ยาทาแผลในช่องปาก',
            'amount'     => 100,
            'type'       => 'ซอง'
        ]);

        Medicine::create([
            'code'       => "MC010",
            'name'       => '0.12% Chlorhexidine mouth wash 240ml',
            'properties' => 'ยาบ้วนปาก',
            'amount'     => 100,
            'type'       => 'ขวด'
        ]);
    }
}
