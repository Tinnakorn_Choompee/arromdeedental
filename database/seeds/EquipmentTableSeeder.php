<?php

use Illuminate\Database\Seeder;
use App\Models\Equipment;

class EquipmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Equipment::create([
            'code'       => "EM001",
            'name'       => 'ฟันยาง',
            'properties' => 'อุปกรณ์ที่สวมเพื่อปกป้องฟัน',
            'amount'     => 5,
            'type'       => 'อัน'
        ]);

        Equipment::create([
            'code'       => "EM002",
            'name'       => 'วัสดุอุดฟัน',
            'properties' => 'วัสดุสีเหมือนฟัน',
            'amount'     => 10,
            'type'       => 'ชุด'
        ]);
    }
}
