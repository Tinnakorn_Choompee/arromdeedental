<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolePermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::where(['name' => "admin"])->first();
        $admin->syncPermissions([range(1, 47),50]);

        $dentist = Role::where(['name' => "dentist"])->first();
        $dentist->syncPermissions([1,5,9,13,17,37,range(50, 53)]);

        $authority = Role::where(['name' => "authority"])->first();
        $authority->syncPermissions([1,5,9, range(10,13), 17, range(29,46), 50]);
    }
}
