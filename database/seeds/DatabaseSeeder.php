<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(RolePermissionTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(MemberTableSeeder::class);
        $this->call(UnitTableSeeder::class);
        $this->call(MedicinesTableSeeder::class);
        $this->call(EquipmentTableSeeder::class);
        $this->call(FeeTableSeeder::class);
    }
}
