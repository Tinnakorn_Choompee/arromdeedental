<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // Admin
       $user           = new User;
       $user->name     = "Administator";
       $user->username = "theadmin";
       $user->password = "123456";
       $user->email    = "admin@gmail.com";
       $user->phone    = "0918595385";
       $user->address  = "Sansai";
       $user->save();
       $user->assignRole("admin");

       // Dentist
       $dentist           = new User;
       $dentist->name     = "Dentist";
       $dentist->username = "dentist";
       $dentist->password = "123456";
       $dentist->email    = "dentist@gmail.com";
       $dentist->phone    = "0918595386";
       $dentist->address  = "MaeRim";
       $dentist->save();
       $dentist->assignRole("dentist");

       $dentist           = new User;
       $dentist->name     = "Dentist2";
       $dentist->username = "dentist2";
       $dentist->password = "123456";
       $dentist->email    = "dentist2@gmail.com";
       $dentist->phone    = "0918595322";
       $dentist->address  = "MaeRim2";
       $dentist->save();
       $dentist->assignRole("dentist");


       // Authority
       $authority           = new User;
       $authority->name     = "Authority";
       $authority->username = "users";
       $authority->password = "123456";
       $authority->email    = "authority@gmail.com";
       $authority->phone    = "0918595387";
       $authority->address  = "Doisutep";
       $authority->save();
       $authority->assignRole("authority");

       $authority           = new User;
       $authority->name     = "Authority2";
       $authority->username = "users2";
       $authority->password = "123456";
       $authority->email    = "authority2@gmail.com";
       $authority->phone    = "0918595311";
       $authority->address  = "Doisutep2";
       $authority->save();
       $authority->assignRole("authority");
    }
}