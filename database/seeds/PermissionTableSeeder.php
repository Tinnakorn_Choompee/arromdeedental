<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'authority-list',
            'authority-create',
            'authority-edit',
            'authority-delete',

            'dentist-list',
            'dentist-create',
            'dentist-edit',
            'dentist-delete',

            'member-list',
            'member-create',
            'member-edit',
            'member-delete',

            'medicines-list',
            'medicines-create',
            'medicines-edit',
            'medicines-delete',

            'equipment-list',
            'equipment-create',
            'equipment-edit',
            'equipment-delete',

            'type_medicines-list',
            'type_medicines-create',
            'type_medicines-edit',
            'type_medicines-delete',

            'type_equipment-list',
            'type_equipment-create',
            'type_equipment-edit',
            'type_equipment-delete',

            'reimbursement_medicines-list',
            'reimbursement_medicines-create',
            'reimbursement_medicines-edit',
            'reimbursement_medicines-delete',

            'reimbursement_equipment-list',
            'reimbursement_equipment-create',
            'reimbursement_equipment-edit',
            'reimbursement_equipment-delete',

            'appointments-list',
            'appointments-create',
            'appointments-edit',
            'appointments-delete',
            
            'payments-list',

            'stock_medicines-list',
            'stock_medicines-create',
            'stock_medicines-delete',

            'stock_equipment-list',
            'stock_equipment-create',
            'stock_equipment-delete',

            'role-list',  
            'role-edit',

            'treatments-list',
            'treatments-create',
            'treatments-edit',
            'treatments-delete',
        ];

        foreach ($permissions as $k => $permission) :
            Permission::create(['name'=> $permission]);
        endforeach;
    }
}
