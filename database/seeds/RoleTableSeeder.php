<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleTableSeeder extends Seeder
{
    public function run()
    {
        $admin     = Role::create(['name' => "admin"]);
        $dentist   = Role::create(['name' => "dentist"]);
        $authority = Role::create(['name' => "authority"]);
    }
}
