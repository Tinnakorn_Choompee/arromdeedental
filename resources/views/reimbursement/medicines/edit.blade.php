@extends('layouts.app') 
@section('title', 'แก้ไขการเบิกคืนยา') 
@section('content')
@include('layouts.template.header', ['icon'=>'icon-aid-kit', 'name' => 'แก้ไขการเบิกคืนยา'])
<div class="content">
    {{ Form::model($reimbursement_medicine, ['method'=>'PATCH', 'route'=> ['reimbursement_medicines.update', $reimbursement_medicine->id], 'id' => 'FormValidation'])}}
    @include('layouts.pattern.errors', ['errors' => $errors])
    <div class="card">
        <div class="card-body">
            <fieldset class="mb-3">
                <legend class="text-uppercase font-weight-bold font">
                    รายละเอียดการเบิกคืนยา
                </legend>
                <div class="form-group row">
                    {{ Form::label('medicine_id', 'ชื่อยา', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::hidden('medicine_id', $reimbursement_medicine->medicine_id) }}
                        {{ Form::text('name', $reimbursement_medicine->medicine->name , ['class' => 'form-control', 'readonly'] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('code', 'รหัสยา', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::text('code', $reimbursement_medicine->medicine->code , ['class' => 'form-control', 'readonly'] ) }}
                    </div>
                </div>
                <div class="form-group row form-group-feedback form-group-feedback-right">
                    {{ Form::label('disburse', 'จำนวนเบิกจ่าย', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::number('disburse', null, ['class' => 'form-control phone'] ) }}
                        <div class="form-control-feedback form-control-feedback-lg mr-2">
                            <span class="unit"> {{ $reimbursement_medicine->medicine->type }} </span>
                        </div>
                        <span class="form-text text-muted balance"> คงเหลือ  <span id="amount"> {{ $reimbursement_medicine->medicine->amount + $reimbursement_medicine->disburse }}  </span> <span class="unit"> {{ $reimbursement_medicine->medicine->type }} </span> </span>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="text-center">
                <button type="submit" class="btn btn-primary font"> บันทึก <i class="icon-paperplane ml-2"></i></button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>
@endsection
@push('scripts')
    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    document.addEventListener('DOMContentLoaded', function(e) {
        const form = document.getElementById('FormValidation');
        FormValidation.formValidation(form, {
            fields: {
                disburse: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณากรอกจำนวนเบิกจ่าย'
                        },
                        numeric: {
                            message: 'The value is not a number',
                            thousandsSeparator: '',
                            decimalSeparator: '.'
                        }
                    }
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap(),
                submitButton: new FormValidation.plugins.SubmitButton(),
                defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            },
        });
    });
    </script>
@endpush