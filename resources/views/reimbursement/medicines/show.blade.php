@extends('layouts.app') 
@section('title', 'ข้อมูลประเภทยา') 
@section('content')
@include('layouts.template.header', ['icon'=>'icon-aid-kit', 'name' => 'ข้อมูลประเภทยา'])
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-body border-top-info">
                <h6 class="mb-3 font-weight-semibold font f-22">
                    รายละเอียดการเบิกคืนยา
                    @switch($reimbursement_medicine->status)
                        @case('disburse')
                            <span class="float-right badge badge-flat border-warning text-warning-600 font f-20"> เบิกจ่าย </span>
                        @break
                        @case('takeback')
                            <span class="float-right badge badge-flat border-success text-success-600 font f-20"> ส่งคืน </span>
                        @break
                    @endswitch
                </h6>
                <div class="card">
                    <div class="card-body text-center">
                        <h6 class="font-weight-semibold mb-0 font f-22"> <strong> รหัสยา </strong> </h6>
                        <span class="d-block text-muted font f-20"> {{ $reimbursement_medicine->medicine->code }} </span>
                    </div>
                </div>
                <div class="card card-body bg-light mb-0">
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                                <strong> ชื่อยา </strong>
                            </dt>
                        <dd class="col-sm-3">
                            {{ $reimbursement_medicine->medicine->name }}
                        </dd>
                    </dl>
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                                <strong> จำนวนเบิกจ่าย </strong>
                            </dt>
                        <dd class="col-sm-3">
                            {{ $reimbursement_medicine->disburse }}
                        </dd>
                    </dl>
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                                <strong> จำนวนรับคืน </strong>
                            </dt>
                        <dd class="col-sm-3">
                            {{ $reimbursement_medicine->takeback }}
                        </dd>
                    </dl>
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                                <strong> จำนวนคงเหลือ </strong>
                            </dt>
                        <dd class="col-sm-3">
                            {{ $reimbursement_medicine->balance }}
                        </dd>
                    </dl>
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> วันที่เบิกจ่าย </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ date_thai($reimbursement_medicine->created_at) }}
                        </dd>
                    </dl>
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> วันที่รับคืน </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ $reimbursement_medicine->datetime ? date_thai($reimbursement_medicine->datetime): NULL }}
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection