@extends('layouts.app')
@section('title', 'Reimbursement Equipment | การเบิกอุปกรณ์')
@section('content')
@include('layouts.template.header',['icon'=> 'icon-hammer-wrench', 'name' => 'การเบิกคืนอุปกรณ์'])
<div class="content">
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title font"> ข้อมูลการเบิกอุปกรณ์ </h5>
            @can('reimbursement_equipment-create')
            <a href="{{ route('reimbursement_equipment.create') }}" class="btn alpha-blue text-blue-800 border-blue-600 font" style="font-size:18px">
                <i class="icon-folder-plus3 mr-2"></i> เพิ่มการเบิกอุปกรณ์
            </a>
            @endcan
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered table-striped datatable-reimbursement">
                    <thead>
                        <tr class="bg-slate-600 text-center">
                            <th class="text-center">ลำดับ</th>
                            <th> รหัส </th>
                            <th> ชื่อ </th>
                            <th> จำนวนเบิกจ่าย </th>
                            {{-- <th> จำนวนส่งคืน </th> --}}
                            <th> จำนวนคงเหลือ </th>
                            {{-- <th> สถานะ </th> --}}
                            <th> วันที่เบิกจ่าย </th>
                            {{-- <th> วันที่ส่งคืน </th> --}}
                            <th> กระทำ </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($equipment as $k => $rs)
                        <tr>
                            <td class="text-center"> {{ ++$k }}</td>
                            <td class="text-center">{{ $rs->equipment->code }}</td>
                            <td>{{ $rs->equipment->name }}</td>
                            <td class="text-center">{{ $rs->disburse }}</td>
                            {{-- <td class="text-center">{{ $rs->takeback }}</td> --}}
                            <td class="text-center">{{ $rs->balance  }}</td>
                            {{-- <td class="text-center">
                                @switch($rs->status)
                                    @case('disburse')
                                    <span class="badge badge-flat border-warning text-warning-600 font f-20"> เบิกจ่าย </span>
                                    @break
                                    @case('takeback')
                                    <span class="badge badge-flat border-success text-success-600 font f-20"> ส่งคืน </span>
                                    @break
                                @endswitch
                            </td> --}}
                            <td class="text-center">{{ date_thai($rs->created_at) }}</td>
                            {{-- <td class="text-center">{{ $rs->datetime ? date_thai($rs->datetime): NULL }}</td> --}}
                            <td class="center">
                                <div class="list-icons">
                                    @switch($rs->status)
                                        @case('disburse')
                                            {{-- <button class="btn btn-success" 
                                            data-id="{{ $rs->id }}" 
                                            data-toggle="modal" 
                                            data-target="#modal_form" 
                                            data-popup="tooltip" 
                                            title="ส่งคืน" 
                                            data-placement="bottom"
                                            >
                                            <i class="icon-folder-download2"></i>
                                            </button> --}}
                                            <a href="{{ route('reimbursement_equipment.show', $rs->id) }}" data-popup="tooltip" title="รายละเอียด" data-placement="bottom" class="btn btn-primary" title="ดูข้อมูล">
                                                <i class="icon-eye"></i>
                                            </a>
                                            @can('reimbursement_equipment-edit')
                                            <a class="btn btn-warning btn-edit" href="{{ route('reimbursement_equipment.edit',$rs->id) }}" data-popup="tooltip" title="แก้ไข" data-placement="bottom">
                                                <i class="icon-pencil7"></i>
                                            </a>
                                            @endcan
                                            @can('reimbursement_equipment-delete')
                                            <button class="btn btn-danger  btn-del" data-id="{{ $rs->id }}" data-popup="tooltip" title="ลบ" data-placement="bottom">
                                                <i class="icon-trash"></i>
                                            </button>
                                            {{ Form::open(['method' => 'DELETE', 'route' => ['reimbursement_equipment.destroy', $rs->id], 'id'=>'delete-'.$rs->id])}} {{ Form::close() }}
                                            @endcan
                                        @break
                                        @case('takeback')
                                            <a href="{{ route('reimbursement_equipment.show', $rs->id) }}" data-popup="tooltip" title="รายละเอียด" data-placement="bottom" class="btn btn-primary" title="ดูข้อมูล">
                                                <i class="icon-eye"></i>
                                            </a>
                                            @can('reimbursement_equipment-delete')
                                                <button class="btn btn-danger  btn-del" data-id="{{ $rs->id }}" data-popup="tooltip" title="ลบ" data-placement="bottom">
                                                    <i class="icon-trash"></i>
                                                </button>
                                                {{ Form::open(['method' => 'DELETE', 'route' => ['reimbursement_equipment.destroy', $rs->id], 'id'=>'delete-'.$rs->id])}} {{ Form::close() }}
                                            @endcan
                                        @break
                                    @endswitch
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <br>
            </div>
        </div>
    </div>
</div>
<!-- Reimbursement form modal -->
<div id="modal_form" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"> จำนวนส่งคืน </h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            {{ Form::open(['route'=>'reimbursement_equipment.takeback', 'method' => 'PATCH']) }}
                <div class="modal-body">
                    @if($errors->any())
                    <div class="alert alert-danger text-center">
                        @foreach ($errors->all() as $error)
                            <p class="font fs-20">{!! $error !!}</p>
                        @endforeach
                    </div>
                    @endif
                    {{ Form::hidden('id', null, ['id'=>'id']) }}
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                {{ Form::number('takeback', 0, ['class' => 'form-control', 'autofocus', 'required', 'id'=>'takeback'] ) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link font" data-dismiss="modal"> ปิด </button>
                    <button type="submit" class="btn bg-primary font"> บันทึก </button>
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
<!-- /Reimbursement form modal -->
@endsection

@push('scripts')
  <script>
    @if(count($errors) > 0)
        $('#modal_form').modal('show');
    @endif
    
    $('#modal_form').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        $(this).find('.modal-body #id').val(id)
    });
  </script>  
@endpush
