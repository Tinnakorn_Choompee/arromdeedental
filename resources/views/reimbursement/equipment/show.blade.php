@extends('layouts.app') 
@section('title', 'ข้อมูลการเบิกอุปกรณ์') 
@section('content')
@include('layouts.template.header', ['icon'=>'icon-hammer-wrench', 'name' => 'ข้อมูลประเภทอุปกรณ์'])
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-body border-top-info">
                <h6 class="mb-3 font-weight-semibold font f-22">
                    รายละเอียดการเบิกอุปกรณ์
                    {{-- @switch($reimbursement_equipment->status)
                        @case('disburse')
                            <span class="float-right badge badge-flat border-warning text-warning-600 font f-20"> เบิกจ่าย </span>
                        @break
                        @case('takeback')
                            <span class="float-right badge badge-flat border-success text-success-600 font f-20"> ส่งคืน </span>
                        @break
                    @endswitch --}}
                </h6>
                <div class="card">
                    <div class="card-body text-center">
                        <h6 class="font-weight-semibold mb-0 font f-22"> <strong> รหัสอุปกรณ์ </strong> </h6>
                        <span class="d-block text-muted font f-20"> {{ $reimbursement_equipment->equipment->code }} </span>
                    </div>
                </div>
                <div class="card card-body bg-light mb-0">
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                                <strong> ชื่ออุปกรณ์ </strong>
                            </dt>
                        <dd class="col-sm-3">
                            {{ $reimbursement_equipment->equipment->name }}
                        </dd>
                    </dl>
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                                <strong> จำนวนเบิกจ่าย </strong>
                            </dt>
                        <dd class="col-sm-3">
                            {{ $reimbursement_equipment->disburse }}
                        </dd>
                    </dl>
                    {{-- <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                                <strong> จำนวนรับคืน </strong>
                            </dt>
                        <dd class="col-sm-3">
                            {{ $reimbursement_equipment->takeback }}
                        </dd>
                    </dl> --}}
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                                <strong> จำนวนคงเหลือ </strong>
                            </dt>
                        <dd class="col-sm-3">
                            {{ $reimbursement_equipment->balance }}
                        </dd>
                    </dl>
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> วันที่เบิกจ่าย </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ date_thai($reimbursement_equipment->created_at) }}
                        </dd>
                    </dl>
                    {{-- <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> วันที่รับคืน </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ $reimbursement_equipment->datetime ? date_thai($reimbursement_equipment->datetime): NULL }}
                        </dd>
                    </dl> --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection