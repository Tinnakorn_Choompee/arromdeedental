@extends('layouts.app')
@section('title', 'เพิ่มการเบิกอุปกรณ์')
@section('content')
@include('layouts.template.header', ['icon'=>'icon-hammer-wrench', 'name' => 'เพิ่มการเบิกอุปกรณ์'])
<div class="content">
    {{ Form::open(['route'=>'reimbursement_equipment.store', 'id' => 'FormValidation']) }}
    @include('layouts.pattern.errors', ['errors' => $errors])
    <div class="card">
        <div class="card-body">
            <fieldset class="mb-3">
                <legend class="text-uppercase font-weight-bold font">
                    รายละเอียดการเบิกอุปกรณ์
                </legend>
                <div class="form-group row">
                    {{ Form::label('equipment_id', 'ชื่ออุปกรณ์', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::select('equipment_id', $equipment ,  NULL, ['class' => 'form-control select-search', 'placeholder'=>'-- เลือกชื่ออุปกรณ์ --'] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('code', 'รหัสอุปกรณ์', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::text('code', null, ['class' => 'form-control', 'readonly'] ) }}
                    </div>
                </div>
                <div class="form-group row form-group-feedback form-group-feedback-right">
                    {{ Form::label('disburse', 'จำนวนเบิกจ่าย', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::number('disburse', null, ['class' => 'form-control phone'] ) }}
                        <div class="form-control-feedback form-control-feedback-lg mr-2">
                            <span class="unit"></span>
                        </div>
                        <span class="form-text text-muted balance"> คงเหลือ  <span id="amount">  </span> <span class="unit"></span> </span>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="text-center">
                <button type="submit" class="btn btn-primary font"> บันทึก <i class="icon-paperplane ml-2"></i></button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>
@endsection
@push('scripts')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.balance').hide()

    $('#equipment_id').on('change', function(e){
        var equipment_id = e.target.value;
        if(equipment_id) {
            $('#disburse').prop("disabled", false);
            $.ajax({
                type: "GET",
                url: "/json-equipment/"+equipment_id,
                success: function(rs){
                    $('#equipment_id').val(rs.id)
                    $('#code').val(rs.code)
                    $('.unit').text(rs.type)
                    $('#amount').text(rs.amount)
                    $('.balance').show()
                }
            });
        } else {
            $('.balance').hide()
            $('#code').val("")
            $('#disburse').val("")
            $('.unit').text("")
            $('.amount').text("")
            $('#disburse').prop("disabled", true);
        }
    });

    document.addEventListener('DOMContentLoaded', function(e) {
        const form = document.getElementById('FormValidation');
        FormValidation.formValidation(form, {
            fields: {
                equipment_id: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณาเลือกชื่ออุปกรณ์'
                        }
                    }
                },
                disburse: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณากรอกจำนวนเบิกจ่าย'
                        },
                        numeric: {
                            message: 'The value is not a number',
                            thousandsSeparator: '',
                            decimalSeparator: '.'
                        }
                    }
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap(),
                submitButton: new FormValidation.plugins.SubmitButton(),
                defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            },
        });
    });
</script>
@endpush
