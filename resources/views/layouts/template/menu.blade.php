<div class="sidebar sidebar-dark sidebar-expand-md">
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
			<i class="icon-arrow-left8"></i>
		</a> <span class="font">เมนู</span>
        <a href="#" class="sidebar-mobile-expand">
			<i class="icon-screen-full"></i>
			<i class="icon-screen-normal"></i>
		</a>
    </div>
    <div class="sidebar-content">
        <div class="sidebar-user">
            <div class="card-body">
                <div class="media">
                    <div class="mr-3">
                        <a href="{{ route('profile.index') }}">
                            {!! Html::image('image/users/'.Auth::user()->image,NULL, ['class'=>'rounded-circle' , 'width' => '50']) !!}
                        </a>
                    </div>
                    <div class="media-body">
                        <div class="media-title font-weight-semibold f-16"> {!! Auth::user()->username !!} </div>
                        <div class="font-size-xs opacity-10 mt-2">
                            @if(!empty(Auth::user()->getRoleNames())) @foreach(Auth::user()->getRoleNames() as $v)
                            <label class="badge badge-success f-12">{{ $v }}</label> @endforeach @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">
                <li class="nav-item">
                    <a href="{{ route('home') }}" class="nav-link {{ starts_with(Request::route()->getName(), 'home') ? 'active' : '' }}">
                        <i class="icon-home2"></i>
                        <span class="font">
                            หน้าหลัก
                        </span>
                    </a>
                </li>
                @foreach(user() as $k => $v)
                    @can($v['role'])
                    <li class="nav-item">
                        <a href="{{ $v['route'] }}" class="nav-link {{ starts_with(Request::route()->getName(), $k) ? 'active' : '' }}">
                            <i class="fa {{ $v['icon'] }}"></i>
                            <span class="font">
                                {{ $v['name'] }}
                            </span>
                        </a>
                    </li>
                    @endcan
                @endforeach
                @foreach(menu() as $k => $v)
                    @if($v['submenu'] == FALSE)
                        @can($v['role'])
                        <li class="nav-item">
                            <a href="{{ $v['route'] }}" class="nav-link {{ starts_with(Request::route()->getName(), $k) ? 'active' : '' }}">
                                <i class="fa {{ $v['icon'] }}"></i>
                                <span class="font">
                                    {{ $v['name'] }}
                                </span>
                                @if ($k == "payments")
                                    @if(payment_count() != 0)
                                    <span class="badge bg-danger-400 align-self-center ml-auto"> {{ payment_count() }} </span>
                                    @endif
                                @endif
                            </a>
                        </li>
                        @endcan
                    @else
                        @if(Auth::user()->hasAnyPermission($v['permission']))
                        <li class="nav-item nav-item-submenu {{ str_contains(Request::route()->getName(), $k) ? 'nav-item-expanded nav-item-open' : '' }}">
                            <a href="#" class="nav-link"><i class="fa {{ $v['icon'] }}"></i> <span class="font"> {{ $v['title'] }} </span></a>
                            <ul class="nav nav-group-sub" data-submenu-title="{{ $v['title'] }}">
                                @foreach($v['list'] as $name => $list)
                                    @can($list['role'])
                                        <li class="nav-item"><a href="{{ $list['route'] }}" class="nav-link font {{ starts_with(Request::route()->getName(), $name) ? 'active' : '' }}"> {!! $list['name'] !!} </a></li>
                                    @endcan
                                @endforeach
                            </ul>
                        </li>
                        @endif
                    @endif
                @endforeach
                <li class="nav-item">
                    <a href="{{ route('profile.index') }}" class="nav-link {{ starts_with(Request::route()->getName(), 'profile') ? 'active' : '' }}">
						<i class="icon-profile"></i>
						<span class="font">
						 โปรไฟล์
						</span>
					</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link" onclick="event.preventDefault();document.getElementById('logout-form-menu').submit();">
                        {!! Form::open(['route' => 'logout', 'id'=>'logout-form-menu']) !!} {!! Form::close() !!}
						<i class="icon-switch2"></i>
						<span class="font">
						 ออกจากระบบ
						</span>
					</a>
                </li>
            </ul>
        </div>
    </div>
</div>

