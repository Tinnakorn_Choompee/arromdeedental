<div class="navbar navbar-expand-md navbar-dark">
    <div class="navbar-brand">
        <a href="{{ route('home') }}" class="d-inline-block">
            {!! Html::image('image/banner.png') !!}
        </a>
    </div>
    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>
    <div class="collapse navbar-collapse" id="navbar-mobile">
        <span class="navbar-text ml-md-3 mr-md-auto">
            <span class="badge bg-success"></span>
        </span>
        <ul class="navbar-nav">
            @role('admin|authority')
            @if(payment_count() != 0)
            <li class="nav-item dropdown">
                <a href="#" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown" aria-expanded="false">
                    <i class="icon-bell2"></i>
                    <span class="badge badge-pill bg-warning-400 ml-auto ml-md-0"> {{ payment_count()  }}</span>   
            	</a>
                <div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">
                    <div class="dropdown-content-body dropdown-scrollable">
                        <ul class="media-list">
                            @foreach (payment_proceed() as $k => $item)
                            <li class="media font">
                                <div class="media-body">
                                    <div class="media-title">
                                        <a data-fancybox data-type="iframe" data-src="{{ route('payments.show', $item->id) }}" href="javascript:;">
                                            <span class="font-weight-semibold"> {{ $item->treatment->title }} </span>
                                            <span class="text-muted float-right"> {{ date_thai($item->created_at, TRUE) }} </span>
            							</a>
                                    </div>
                                    <span class="text-muted">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <strong> หมอ </strong>
                                            </div>
                                            <div class="col-md-8">
                                                <p> {{ $item->treatment->dentist->name  }} </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <strong> ผู้รับบริการ </strong>
                                            </div>
                                            <div class="col-md-8">
                                                <p> {{ $item->treatment->member->name  }} </p>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="dropdown-content-footer justify-content-center p-0">
                        <a href="{{ route('payments.index') }}" class="bg-light text-grey w-100 py-2">
                            <span class="text-center d-block top-0 font"> การชำระเงินทั้งหมด </span>
                        </a>
                    </div>
                </div>
            </li>
            @endif
            @endrole
            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                    {!! Html::image('image/users/'.Auth::user()->image, NULL, ['class'=>'rounded-circle', 'width'=>'35']) !!}
                    <span> {{ Auth::user()->username }} </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right font">
                    <a href="{{ route('profile.index') }}" class="dropdown-item"><i class="icon-profile"></i> โปรไฟล์  </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item font" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="icon-switch2"></i> ออกจากระบบ </a>
                    {!! Form::open(['route' => 'logout', 'id'=>'logout-form']) !!} {!! Form::close() !!}
                </div>
            </li>
        </ul>
    </div>
</div>
