<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link rel="icon" href="/image/favicon.png" type="image/x-icon"/>
    {!! Html::style('template/plugins/formvalidation/dist/css/formValidation.min.css') !!}
    {!! Html::style("template/assets/css/icons/icomoon/styles.css") !!}
    {!! Html::style("template/assets/css/icons/fontawesome/styles.min.css") !!}
    {!! Html::style("template/assets/css/bootstrap.min.css")!!}
    {!! Html::style("template/assets/css/bootstrap_limitless.min.css") !!}
    {!! Html::style("template/assets/css/layout.min.css")!!}
    {!! Html::style("template/assets/css/components.min.css") !!}
    {!! Html::style("template/assets/css/colors.min.css") !!}
    {!! Html::style('template/plugins/flatpickr/flatpickr.min.css') !!}
    {!! Html::style('template/plugins/clockpicker/clockpicker.css') !!}
    {!! Html::style('template/plugins/fancybox-master/dist/jquery.fancybox.min.css') !!} 
    {!! Html::style("css/font.css") !!}
    {!! Html::style("css/style.css") !!}
    @stack('styles')
    {!! Html::script("template/assets/js/main/jquery.min.js") !!}
    {!! Html::script("template/assets/js/main/bootstrap.bundle.min.js") !!}
    {!! Html::script("template/assets/js/plugins/loaders/blockui.min.js") !!}
    {!! Html::script("template/assets/js/plugins/ui/ripple.min.js") !!}
    {!! Html::script("template/assets/js/plugins/notifications/sweet_alert.min.js")!!}
    {!! Html::script("template/assets/js/plugins/ui/perfect_scrollbar.min.js")!!}
    {!! Html::script("template/assets/js/plugins/forms/styling/uniform.min.js")!!}
    {!! Html::script("template/assets/js/plugins/forms/selects/select2.min.js")!!}
    {!! Html::script("template/assets/js/plugins/forms/selects/bootstrap_multiselect.js")!!}
    {!! Html::script("template/assets/js/plugins/tables/datatables/datatables.min.js")!!}
    {!! Html::script('template/plugins/formvalidation/dist/js/FormValidation.min.js')    !!}
    {!! Html::script('template/plugins/formvalidation/dist/js/plugins/Bootstrap.min.js') !!}
    {!! Html::script('template/plugins/flatpickr/flatpickr.js') !!}
    {!! Html::script('template/plugins/flatpickr/th.js') !!}
    {!! Html::script('template/plugins/clockpicker/clockpicker.js') !!}
    {!! Html::script('template/plugins/fancybox-master/dist/jquery.fancybox.min.js') !!} 
    {!! Html::script("template/assets/js/app.js") !!}
</head>
<body>
    @auth
        @include('layouts.template.navbar')
        <div class="page-content">
            @include('layouts.template.menu')
            <div class="content-wrapper">
                @yield('content')
                @include('layouts.template.footer')
            </div>
        </div>
        {!! Html::script("template/assets/js/custom.js") !!}
        @include('layouts.pattern.alert')
    @endauth

    @guest
        @yield('login')
    @endguest

    @stack('scripts')
</body>
</html>
