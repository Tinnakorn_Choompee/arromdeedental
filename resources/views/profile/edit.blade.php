@extends('layouts.app')
@section('title', 'แก้ไขโปรไฟล์')
@section('content')
@include('layouts.template.header', ['icon'=>'icon-user','name' => 'แก้ไขโปรไฟล์'])
<div class="content">
    {{ Form::open(['route' => 'profile.update', 'method' => 'PATCH', 'id' => 'FormValidation', 'files' => TRUE]) }}
    @include('layouts.pattern.errors', ['errors' => $errors])
    <div class="card">
        <div class="card-body">
            <fieldset class="mb-3">
                <legend class="text-uppercase font-weight-bold font">
                    รายละเอียดโปรไฟล์
                    <span class="float-right">
                        {{ Html::image('image/users/'.Auth::user()->image, NULL , ['width'=>'60','height'=>'55', 'class'=>'rounded', 'id'=> 'photo']) }}
                    </span>
                </legend>
                <div class="form-group row">
                    {{ Form::label('name', 'ชื่อ', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::text('name', Auth::user()->name, ['class' => 'form-control'] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('username', 'ชื่อเข้าสู่ระบบ', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::text('username', Auth::user()->username, ['class' => 'form-control'] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('email', 'อีเมล์', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::email('email', Auth::user()->email, ['class' => 'form-control'] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('phone', 'เบอร์โทร', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::text('phone', Auth::user()->phone, ['class' => 'form-control phone', 'maxlength' => 10] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('image', 'เลือกรูป', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::hidden('edit_image', Auth::user()->image) }}
                        {{ Form::file('image', ['class' => 'form-control-uniform-custom', 'id'=>'ActiveImage'] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('address', 'ที่อยู่', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::textarea('address', Auth::user()->address, ['rows' => 4, 'class' => 'form-control'] ) }}
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <fieldset class="mb-3">
                <legend class="text-uppercase font-weight-bold font"> รหัสผ่าน </legend>
                <div class="form-group row">
                    {{ Form::label('old_password', 'รหัสผ่านเดิม', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::password('old_password', ['class' => 'form-control'] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('password', 'รหัสผ่านใหม่', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::password('password', ['class' => 'form-control'] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('password_confirmation', 'ยืนยันรหัสผ่าน', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::password('password_confirmation', ['class' => 'form-control'] ) }}
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="text-center">
                <button type="submit" class="btn btn-primary font"> บันทึก <i class="icon-paperplane ml-2"></i></button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>
@endsection
@push('scripts')
<script>
    document.addEventListener('DOMContentLoaded', function(e) {
        const form = document.getElementById('FormValidation');
        FormValidation.formValidation(form, {
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณากรอกชื่อ'
                        }
                    }
                },
                username: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณากรอกชื่อผู้ใช้งาน'
                        },
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: 'กรุณากรอกตัวอักษรระหว่าง 6 ถึง 30 ตัวอักษร'
                        }
                    }
                },
                password_confirmation: {
                    validators: {
                        identical: {
                            compare: function() {
                                return form.querySelector('[name="password"]').value;
                            },
                            message: 'พาสเวิร์ดยืนยันไม่ตรงกัน'
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณากรอกอีเมล์'
                        },
                        emailAddress: {
                            message: 'กรุณากรอกข้อมูลอีเมล์'
                        }
                    }
                },
                phone: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณากรอกเบอร์โทร'
                        },
                        stringLength: {
                            min: 10,
                            message: 'กรุณากรอกเบอร์โทร 10 หลัก'
                        }
                    }
                },
                image: {
                    validators: {
                        file: {
                            extension: 'jpeg,jpg,png',
                            type: 'image/jpeg,image/png',
                            message: 'กรุณาเลือกไฟล์รูปภาพ'
                        }
                    }
                }
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap(),
                submitButton: new FormValidation.plugins.SubmitButton(),
                defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            },
        });
    });
</script>
@endpush
