@extends('layouts.app')
@section('title', 'Authority | เจ้าหน้าที่')
@section('content')
@include('layouts.template.header',['icon'=> 'icon-users', 'name' => 'เจ้าหน้าที่'])
<div class="content">
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title font">ข้อมูลเจ้าหน้าที่</h5>
            @can('authority-create')
            <a href="{{ route('authority.create') }}" class="btn alpha-blue text-blue-800 border-blue-600 font" style="font-size:18px">
                <i class="icon-user-plus mr-2"></i> เพิ่มเจ้าหน้าที่
            </a>
            @endcan
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered table-striped datatable-basic">
                    <thead>
                        <tr class="bg-slate-600 text-center">
                            <th class="text-center">ลำดับ</th>
                            <th>ชื่อ</th>
                            <th>อีเมล์</th>
                            <th>เบอร์โทร</th>
                            <th>รูปภาพ</th>
                            <th>กระทำ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($authority as $k => $rs)
                        <tr>
                            <td class="text-center"> {{ ++$k }}</td>
                            <td>{{ $rs->name }}</td>
                            <td>{{ $rs->email }}</td>
                            <td>{{ $rs->phone }}</td>
                            <td class="center">
                                {{ Html::image('image/users/'.$rs->image, NULL, ['width'=>'60','height'=>'55', 'class'=>'rounded img-responsive']) }}
                            </td>
                            <td class="center">
                                @include('layouts.pattern.action', ['name'=> 'authority', 'id' => $rs->id])
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <br>
            </div>
        </div>
    </div>
</div>
@endsection
