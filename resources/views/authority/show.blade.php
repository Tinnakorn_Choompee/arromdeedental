@extends('layouts.app')
@section('title', 'ข้อมูลเจ้าหน้าที่')
@section('content')
@include('layouts.template.header', ['icon'=> 'icon-users', 'name' => 'ข้อมูลเจ้าหน้าที่'])
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-body border-top-info">
                <h6 class="mb-3 font-weight-semibold font f-22">
                    รายละเอียดเจ้าหน้าที่
                </h6>
                <div class="card">
                    <div class="card-body text-center">
                        <div class="card-img-actions d-inline-block mb-3">
                            {{ Html::image('image/users/'.$authority->image, NULL , ['width'=>'150','height'=>'140','class'=>'rounded ']) }}
                        </div>
                        <h6 class="font-weight-semibold mb-0 font f-22"> <strong> เจ้าหน้าที่ </strong> </h6>
                        <span class="d-block text-muted font f-20"> {{ $authority->name }} </span>
                    </div>
                </div>
                <div class="card card-body bg-light mb-0">
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> ชื่อเข้าสู่ระบบ </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ $authority->username }}
                        </dd>
                    </dl>
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> อีเมล์ </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ $authority->email }}
                        </dd>
                    </dl>
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> เบอร์โทร </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ $authority->phone }}
                        </dd>
                    </dl>
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> ที่อยู่ </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ $authority->address }}
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
