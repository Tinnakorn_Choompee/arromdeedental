@extends('layouts.app')
@section('title', 'ข้อมูลผู้รับบริการ')
@section('content')
@include('layouts.template.header', ['icon'=> 'icon-users4', 'name' => 'ข้อมูลผู้รับบริการ'])
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-body border-top-info">
                <h6 class="mb-3 font-weight-semibold font f-22">
                    รายละเอียดผู้รับบริการ
                </h6>
                <div class="card">
                    <div class="card-body text-center">
                        <div class="card-img-actions d-inline-block mb-3">
                            {{ Html::image('image/users/'.$member->image, NULL , ['width'=>'150','height'=>'140','class'=>'rounded ']) }}
                        </div>
                        <h6 class="font-weight-semibold mb-0 font f-22"> <strong> ผู้รับบริการ </strong> </h6>
                        <span class="d-block text-muted font f-20"> {{ $member->name }} </span>
                    </div>
                </div>
                <div class="card card-body bg-light mb-0">

                    @isset($member->gender)
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> เพศ </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ gender($member->gender) }}
                        </dd>
                    </dl>
                    @endisset

                    @isset($member->phone)
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> เบอร์โทร </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ $member->phone }}
                        </dd>
                    </dl>
                    @endisset

                    @isset($member->email)
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> อีเมล์ </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ $member->email }}
                        </dd>
                    </dl>
                    @endisset

                    @isset($member->birthday)
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> วันเกิด </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ date_thai($member->birthday) }}
                        </dd>
                    </dl>
                    @endisset

                    @isset($member->age)
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> อายุ </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ $member->age }}
                        </dd>
                    </dl>
                    @endisset

                    @isset($member->address)
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> ที่อยู่ </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ $member->address }}
                        </dd>
                    </dl>
                    @endisset

                    @isset($member->disease)
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> โรคประจำตัว </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ $member->disease }}
                        </dd>
                    </dl>
                    @endisset

                    @isset($member->allergic)
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> แพ้ยา </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ $member->allergic }}
                        </dd>
                    </dl>
                    @endisset

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
