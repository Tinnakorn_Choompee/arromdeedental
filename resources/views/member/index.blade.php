@extends('layouts.app')
@section('title', 'Member | ผู้รับบริการ')
@section('content')
@include('layouts.template.header',['icon'=> 'icon-users4', 'name' => 'ผู้รับบริการ'])
<div class="content">
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title font"> ข้อมูลผู้รับบริการ </h5>
            @can('member-create')
            <a href="{{ route('member.create') }}" class="btn alpha-blue text-blue-800 border-blue-600 font" style="font-size:18px">
                <i class="icon-user-plus mr-2"></i> เพิ่มผู้รับบริการ
            </a>
            @endcan
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered table-striped datatable-member">
                    <thead>
                        <tr class="bg-slate-600 text-center">
                            <th class="text-center">ลำดับ</th>
                            <th>ชื่อ</th>
                            <th>เบอร์โทร</th>
                            <th>เพศ</th>
                            <th>อีเมล์</th>
                            <th>อายุ</th>
                            <th>รูปภาพ</th>
                            <th>กระทำ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($member as $k => $rs)
                        <tr>
                            <td class="text-center"> {{ ++$k }}</td>
                            <td>{{ $rs->name   }}</td>
                            <td class="text-center">{{ $rs->phone }}</td>
                            <td class="text-center">{{ gender($rs->gender) }}</td>
                            <td>{{ $rs->email  }}</td>
                            <td class="text-center">{{ $rs->age }}</td>
                            <td class="center">
                                {{ Html::image('image/users/'.$rs->image, NULL, ['width'=>'60','height'=>'55', 'class'=>'rounded img-responsive']) }}
                            </td>
                            <td class="center">
                                @include('layouts.pattern.action', ['name'=> 'member', 'id' => $rs->id])
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <br>
            </div>
        </div>
    </div>
</div>
@endsection
