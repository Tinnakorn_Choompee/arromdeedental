@extends('layouts.app')
@section('title', 'เพิ่มผู้รับบริการ')
@section('content')
@include('layouts.template.header', ['icon'=>'icon-users4', 'name' => 'เพิ่มผู้รับบริการ'])
<div class="content">
    {{ Form::open(['route'=>'member.store', 'id' => 'FormValidation', 'files' => TRUE]) }}
    @include('layouts.pattern.errors', ['errors' => $errors])
    <div class="card">
        <div class="card-body">
            <fieldset class="mb-3">
                <legend class="text-uppercase font-weight-bold font">
                    รายละเอียดผู้รับบริการ
                    <span class="float-right">
                        {{ Html::image('image/users/member.png', NULL , ['width'=>'60','height'=>'55', 'class'=>'rounded', 'id'=> 'photo']) }}
                    </span>
                </legend>
                <div class="form-group row">
                    {{ Form::label('name', 'ชื่อผู้รับบริการ', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::text('name', null, ['class' => 'form-control'] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('gender', 'เพศ', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::select('gender', ['Male' => 'ชาย', 'Female' => 'หญิง'],  NULL, ['class' => 'form-control form-control-uniform', 'placeholder'=>'-- เลือกเพศ --'] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('phone', 'เบอร์โทร', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::text('phone', null, ['class' => 'form-control phone', 'maxlength' => 10] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('email', 'อีเมล์', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::email('email', null, ['class' => 'form-control'] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('birthday', 'วันเกิด', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-8">
                        {{ Form::text('birthday', null, ['class' => 'form-control date', 'placeholder' => 'ว/ด/ป'] ) }}
                    </div>
                    <div class="col-2">
                        {{ Form::number('age', null, ['class' => 'form-control', 'readonly', 'id' => 'age', 'placeholder' => 'อายุ']) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('image', 'เลือกรูป', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::file('image', ['class' => 'form-control-uniform-custom', 'id'=> 'ActiveImage'] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('address', 'ที่อยู่', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::textarea('address', null, ['rows' => 3, 'class' => 'form-control'] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('disease', 'โรคประจำตัว', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::textarea('disease', null, ['rows' => 2, 'class' => 'form-control'] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('allergic', 'แพ้ยา', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::textarea('allergic', null, ['rows' => 2, 'class' => 'form-control'] ) }}
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="text-center">
                <button type="submit" class="btn btn-primary font"> บันทึก <i class="icon-paperplane ml-2"></i></button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>
@endsection
@push('scripts')
<script>
    document.addEventListener('DOMContentLoaded', function(e) {
        const form = document.getElementById('FormValidation');
        FormValidation.formValidation(form, {
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณากรอกชื่อผู้รับบริการ'
                        }
                    }
                },
                gender: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณาเลือกเพศ'
                        }
                    }
                },
                phone: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณากรอกเบอร์โทร'
                        }
                    }
                },
                image: {
                    validators: {
                        file: {
                            extension: 'jpeg,jpg,png',
                            type: 'image/jpeg,image/png',
                            message: 'กรุณาเลือกไฟล์รูปภาพ'
                        }
                    }
                }
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap(),
                submitButton: new FormValidation.plugins.SubmitButton(),
                defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            },
        });
    });
</script>
@endpush
