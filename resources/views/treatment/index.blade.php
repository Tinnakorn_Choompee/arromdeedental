@extends('layouts.app')
@section('title', 'Treatments | การรักษา')
@section('content')
@include('layouts.template.header',['icon'=> 'fa-stethoscope f-18', 'name' => 'การรักษา'])
<div class="content">
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title font"> ข้อมูลการรักษา </h5>
            @can('treatments-create')
            <a href="{{ route('treatments.create') }}" class="btn alpha-blue text-blue-800 border-blue-600 font" style="font-size:18px">
                <i class="icon-folder-plus3 mr-2"></i> เพิ่มการรักษา
            </a>
            @endcan
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered table-striped datatable-treatments">
                    <thead>
                        <tr class="bg-slate-600 text-center">
                            <th class="text-center">ลำดับ</th>
                            <th>หัวข้อการรักษา</th>
                            <th>หมอ</th>
                            <th>ผู้รับบริการ</th>
                            <th>วันที่ตรวจรักษา</th>
                            <th>สถานะการจ่ายเงิน</th>
                            <th>กระทำ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($treatments as $k => $rs)
                        <tr>
                            <td class="text-center"> {{ ++$k }}  </td>
                            <td class="text-left"> {{ $rs->title }} </td>
                            <td class="text-center"> {{ $rs->dentist->name }} </td>
                            <td class="text-center"> {{ $rs->member->name }} </td>
                            <td class="text-center"> {{ date_thai($rs->created_at, TRUE) }} </td>
                            <td class="text-center">
                                @switch($rs->payment->status)
                                    @case('proceed')
                                    <span class="badge badge-flat border-warning text-warning-600 font f-16"> กำลังดำเนินการ </span>
                                    @break
                                    @case('success')
                                    <span class="badge badge-flat border-success text-success-600 font f-16"> จ่ายแล้ว </span>
                                    @break
                                @endswitch
                            </td>
                            <td class="center">
                                @include('layouts.pattern.action', ['name'=> 'treatments', 'id' => $rs->id])
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <br>
            </div>
        </div>
    </div>
</div>
@endsection
