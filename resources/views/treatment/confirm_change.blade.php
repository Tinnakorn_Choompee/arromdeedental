@extends('layouts.app')
@section('title', 'ยืนยันการรักษา')
@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>
                <i class="fa fa-stethoscope f-18 mr-2"></i>
                <span class="font-weight-semibold font f-24"> ยืนยันการรักษา </span>
            </h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">หน้าหลัก</a></li>
                <li class="breadcrumb-item"><a href="{{route('treatments.index')}}">การรักษา</a></li>
                <li class="breadcrumb-item"><a href="{{route('treatments.edit', session()->get('id'))}}">แก้ไขการรักษา</a> </li>
                <li class="breadcrumb-item active">ยืนยันการรักษา</li>
            </ol>
        </div>
    </div>
</div>
<!-- /page header -->
<div class="content">
    {{ Form::open(['method'=> 'PATCH', 'route' => ['treatments.change', session()->get('id')], 'id' => 'FormValidation']) }}
    {{ Form::hidden('member_id', session()->get('member_id')) }}
    {{ Form::hidden('film', session()->get('film')) }}
    {{ Form::hidden('title',session()->get('title')) }}
    <div class="card">
        @include('layouts.pattern.errors', ['errors' => $errors])
        <div class="card-body">
            <fieldset class="mb-3">
                <legend class="text-uppercase font-weight-bold font">
                    รายละเอียดการรักษา
                </legend>
                <div class="form-group row">
                    {{ Form::label('title', 'หัวข้อการรักษา', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        <div class="form-control-plaintext ml-4">
                            {{ session()->get('title') }}
                        </div>
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    {{ Form::label('name', 'ผู้รับบริการ', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        <div class="form-control-plaintext ml-4">
                            {{ session()->get('name') }}
                        </div>
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    {{ Form::label('fees', 'รายการที่รักษา', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        @foreach (Session::get('fees') as $k => $item)
                        {{ Form::hidden('id[]', $item['id']) }}
                        {{ Form::hidden('name[]', $item['name']) }}
                        <div class="form-group row">
                            {{ Form::label('price', 'ราคา', ['class'=>'col-form-label col-md-1 text-center']) }}
                            <div class="col-md-1">
                                {{ Form::number('price[]', $item['price'], ['class' => 'form-control phone']) }}
                            </div>
                            <label class="col-form-label col-md-6"> {{ ++$k }}. {{ $item['name'] }}</label>
                        </div>
                        @endforeach
                    </div>
                </div>
                <hr>
                @isset(Session::get('medicines')[0])
                {{ Form::hidden('medicine', TRUE) }}
                <div class="form-group row">
                    {{ Form::label('medicines', 'รายการยาที่เลือก', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        @foreach (Session::get('medicines') as $k => $item)
                        {{ Form::hidden('medicine_id[]', $item['id']) }}
                        {{ Form::hidden('medicine_name[]', $item['name']) }}
                        <div class="form-group row">
                            {{ Form::label('price', 'ราคา', ['class'=>'col-form-label col-md-1 text-center']) }}
                            <div class="col-md-1">
                                {{ Form::number('medicine_price[]', $item['price'], ['class' => 'form-control phone']) }}
                            </div>
                            <label class="col-form-label col-md-6"> {{ ++$k }}. {{ $item['name'] }}</label>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endisset
            </fieldset>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="text-center">
                <a href="{{ url()->previous() }}" class="btn btn-light font"><i class="icon-arrow-left13 mr-2"></i> ย้อนกลับ </a>
                <button type="submit" class="btn btn-primary font"> บันทึกการรักษา <i class="icon-paperplane ml-2"></i></button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>
@endsection
