@extends('layouts.app')
@section('title', 'แก้ไขการรักษา')
@section('content')
@include('layouts.template.header', ['icon'=>'fa-stethoscope f-18', 'name' => 'แก้ไขการรักษา'])
<div class="content">
    {{ Form::model($treatment,['method' => 'PATCH', 'route' => ['treatments.update', $treatment->id], 'id' => 'FormValidation', 'files'=> TRUE]) }}
    @include('layouts.pattern.errors', ['errors' => $errors])
    <div class="card">
        <div class="card-body">
            <fieldset class="mb-3">
                <legend class="text-uppercase font-weight-bold font">
                    รายละเอียดการรักษา
                </legend>
                <div class="form-group row">
                    {{ Form::label('title', 'หัวข้อการรักษา', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::text('title', NULL, ['class' => 'form-control'] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('name', 'ผู้รับบริการ', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::select('member_id', $member ,  NULL, ['class' => 'form-control select-search', 'placeholder'=>'-- เลือกผู้รับบริการ --'] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('image', 'แนบฟิลม์ (ถ้ามี)', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::hidden('edit_image', $treatment->image) }}
                        {{ Form::file('image', ['class' => 'form-control-uniform-custom', 'id'=> 'ActiveImage'] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('fees', 'การรักษา', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-9">
                        <select class="form-control multiselect-full-featured" multiple="multiple" name="fees[]" required>
                            @foreach ($fees as $item)
                                <optgroup label="{{ $item->name }} ({{ $item->title }})">
                                    @foreach ($item->items as $result)
                                        @if(in_array($result->id, $details_treatment))
                                            <option value="{{ $result->id."-".$result->name}}" selected> {{ $result->name }} </option>
                                        @else
                                            <option value="{{ $result->id."-".$result->name}}"> {{ $result->name }} </option>
                                        @endif
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-1">
                        <button type="button" class="btn bg-blue font btn-sm" style="line-height:1.3;" id="reset-fees"> ยกเลิกทั้งหมด </button>
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('medicines', 'รายการยา (ถ้ามี)', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-9">
                        <select class="form-control multiselect-full-medicines" multiple="multiple" name="medicines[]" required>
                            @foreach ($medicines as $item)
                                <optgroup label="{{ $item->name }} ({{ $item->title }})">
                                    @foreach ($item->items as $result)
                                        @empty($details_medicine)
                                            <option value="{{ $result->id."-".$result->name}}"> {{ $result->name }} </option>
                                        @else
                                            @if(in_array($result->id, $details_medicine))
                                                <option value="{{ $result->id."-".$result->name}}" selected> {{ $result->name }} </option>
                                            @else
                                                <option value="{{ $result->id."-".$result->name}}"> {{ $result->name }} </option>
                                            @endif
                                        @endempty

                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-1">
                        <button type="button" class="btn bg-blue font btn-sm" style="line-height:1.3;" id="reset-medicines"> ยกเลิกทั้งหมด </button>
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('fees', 'การรักษาที่เลือก', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        <div class="form-control-plaintext values-empty">
                            ยังไม่มีการรักษาที่เลือก
                        </div>
                        <div class="values-area font mt-2">
                            <ol class='list mb-0'>
                                @foreach($treatment->details_treatment as $rs)
                                    <li class='my-1'> {{ $rs->name }} </li>
                                @endforeach
                            </ol>  
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('fees', 'รายการยาที่เลือก (ถ้ามี)', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        <div class="form-control-plaintext medicines-empty">
                            ยังไม่มีรายการยาที่เลือก
                        </div>
                        <div class="medicines-area font mt-2">
                            <ol class='list mb-0'>
                                @foreach($treatment->details_medicine as $rs)
                                    <li class='my-1'> {{ $rs->name }} </li>
                                @endforeach
                            </ol>  
                        </div>
                    </div>
                </div>
                @isset($treatment->image)
                <div class="form-group row">
                    {{ Form::label('image', 'ฟิลม์ภาพ', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-3">
                        <br>
                        {{ Html::image('image/film/'.$treatment->image, NULL, ['class'=>'img-fluid', 'id'=> 'photo']) }}
                    </div>
                </div>
                @endisset
            </fieldset>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="text-center">
                <button type="submit" class="btn btn-warning font"> ยืนยันการรักษา <i class="icon-warning2 ml-2"></i></button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>
@endsection

@push('styles')
<style>
    ol {
        padding: 0px 12px !important;
    }
</style>
@endpush

@push('scripts')

@empty($treatment->details_medicine[0])
<script>
  $('.medicines-empty').show();
</script>
@else
<script>
  $('.medicines-empty').hide();
</script>
@endempty

<script>
    $('.values-empty').hide();
  

    $('#reset-fees').on('click', function() {
        $('.multiselect-full-featured option:selected').each(function() {
            $(this).prop('selected', false);
        })
        $('.multiselect-full-featured').multiselect('refresh');
        $('.values-empty').show();
        $('.values-area').hide();
    });

    $('.multiselect-full-featured').on('change', function() {
        $('.values-empty').hide();
        $('.values-area').show();
        var text  = "<ol class='list mb-0'>";
        $.each($('.multiselect-full-featured').val(), function(index,value) {
            var items = value.split('-');
            text += "<li class='my-1'>"+items[1]+"</li>";    
        });
        text += "</ol>";
        $('.values-area').html(text);
    });

    $('#reset-medicines').on('click', function() {
        $('.multiselect-full-medicines option:selected').each(function() {
            $(this).prop('selected', false);
        })
        $('.multiselect-full-medicines').multiselect('refresh');
        $('.medicines-empty').show();
        $('.medicines-area').hide();
    });

    $('.multiselect-full-medicines').on('change', function() {
        $('.medicines-empty').hide();
        $('.medicines-area').show();
        var text  = "<ol class='list mb-0'>";
        $.each($('.multiselect-full-medicines').val(), function(index,value) {
            var items = value.split('-');
            text += "<li class='my-1'>"+items[1]+"</li>";    
        });
        text += "</ol>";
        $('.medicines-area').html(text);
    });

    document.addEventListener('DOMContentLoaded', function(e) {
        const form = document.getElementById('FormValidation');
        FormValidation.formValidation(form, {
            fields: {
                title: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณากรอกหัวข้อการรักษา'
                        }
                    }
                },
                member_id: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณาเลือกผู้รับบริการ'
                        }
                    }
                },
                image: {
                    validators: {
                        file: {
                            extension: 'jpeg,jpg,png',
                            type: 'image/jpeg,image/png',
                            message: 'กรุณาเลือกไฟล์รูปภาพ'
                        }
                    }
                }
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap(),
                submitButton: new FormValidation.plugins.SubmitButton(),
                defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            },
        });
    });
</script>
@endpush