@extends('layouts.app') 
@section('title', 'ข้อมูลการรักษา') 
@section('content')
@include('layouts.template.header', ['icon'=>'icon-hammer-wrench', 'name' => 'ข้อมูลการรักษา'])
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-body border-top-info">
                <h6 class="mb-3 font-weight-semibold font f-22">
                    รายละเอียดการรักษา
                </h6>
                <div class="card">
                    <div class="card-body text-center">
                        <h6 class="font-weight-semibold mb-0 font f-22"> <strong> หัวข้อการรักษา </strong> </h6>
                        <span class="d-block text-muted font f-20"> {{ $treatment->title }} </span>
                    </div>
                </div>
                <div class="card card-body bg-light mb-0">
                    {{-- Dentist --}}
                    <dl class="row mb-0">
                        <dt class="col col-sm-1 font text-right">
                            <strong> หมอ </strong>
                        </dt>
                        <dd class="col-sm-10">
                         {{ $treatment->dentist->name }}
                        </dd>
                    </dl>
                    {{-- Member --}}
                    <dl class="row mb-0">
                        <dt class="col col-sm-1 font text-right">
                            <strong> ผู้รับบริการ </strong>
                        </dt>
                        <dd class="col-sm-3">
                         {{ $treatment->member->name }}
                        </dd>
                    </dl>
                    <hr>
                    {{-- Fees --}}
                    <dl class="row mb-0">
                        <dt class="col col-sm-1 font text-right">
                            <strong> รายการที่รักษา </strong>
                        </dt>
                        <dd class="col-sm-10 font">
                            <div class="row">
                                @foreach ($treatment->details_treatment as $item)
                                <div class="col-md-5">
                                    {{ $item->name }}  
                                </div>
                                <div class="col-md-1 text-center">
                                    <b class="mr-3"> ราคา </b>   
                                </div>
                                <div class="col-md-1 text-left">
                                    {{ number_format($item->price) }}
                                </div>
                                <div class="col-md-1 text-center">
                                    <b class="ml-3"> บาท </b>   
                                </div>
                                @endforeach
                            </div>
                        </dd>
                    </dl>

                    @isset($treatment->details_medicine[0])
                    <hr>
                    {{-- Medicines --}}
                    <dl class="row mb-0">
                        <dt class="col col-sm-1 font text-right">
                            <strong> รายการยาที่เลือก </strong>
                        </dt>
                        <dd class="col-sm-10 font">
                            <div class="row">
                                @foreach ($treatment->details_medicine as $item)
                                <div class="col-md-5">
                                    {{ $item->name }}  
                                </div>
                                <div class="col-md-1 text-center">
                                    <b class="mr-3"> ราคา </b>   
                                </div>
                                <div class="col-md-1 text-left">
                                    {{ number_format($item->price) }}
                                </div>
                                <div class="col-md-1 text-center">
                                    <b class="ml-3"> บาท </b>   
                                </div>
                                @endforeach
                            </div>
                        </dd>
                    </dl>                 
                    @endisset
                    <hr>
                    {{-- Total --}}
                    <dl class="row mb-0">
                        <dt class="col col-sm-1 font text-center">
                            <strong> รวมทั้งสิ้น </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ number_format($treatment->total) }}
                            <b class="ml-3 font"> บาท </b>
                        </dd>
                    </dl>

                    @isset($treatment->image)
                    <hr>
                    {{-- Total --}}
                    <dl class="row mb-0">
                        <dt class="col col-sm-1 font text-center">
                            <strong> ฟิลม์ภาพ </strong>
                        </dt>
                        <dd class="col-sm-3">
                            <a href="{{asset("image/film/$treatment->image") }}" data-fancybox="images">
                                {{ Html::image('image/film/'.$treatment->image, NULL, ['class'=>'img-fluid']) }}
                            </a>
                        </dd>
                    </dl>
                    @endisset
                </div>
            </div>
        </div>
    </div>
</div>
@endsection