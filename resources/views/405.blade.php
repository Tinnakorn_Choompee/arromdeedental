@extends('layouts.app')
@section('title', 'หน้าหลัก')
@section('content')
<div class="content justify-content-center align-items-center mt-5">
    <div class="flex-fill"></div>
        <div class="text-center mb-3">
        <h1 class="error-title">405</h1>
        <h5 class="font f-24">  คุณไม่สิทธิ์ในการใช้งานหน้านี้  </h5>
        <br>
        <div class="row">
            <div class="col-xl-4 offset-xl-4 col-md-8 offset-md-2">
                <div class="row font">
                    <div class="col-sm-6">
                        <a href="{{ URL::previous() }}" class="btn btn-light btn-block mt-3 mt-sm-0 f-22"><i class="icon-arrow-left8 mr-2"></i> ย้อนกลับ </a>
                    </div>
                    <div class="col-sm-6">
                        <a href="{{ route('home') }}" class="btn btn-primary btn-block f-22"><i class="icon-home4 mr-2"></i>  หน้าหลัก </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
