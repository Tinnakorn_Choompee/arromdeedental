<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link rel="icon" href="/image/favicon.png" type="image/x-icon" />
    {!! Html::style('template/plugins/formvalidation/dist/css/formValidation.min.css') !!}
    {!! Html::style("template/assets/css/icons/icomoon/styles.css") !!}
    {!! Html::style("template/assets/css/icons/fontawesome/styles.min.css") !!}
    {!! Html::style("template/assets/css/bootstrap.min.css")!!}
    {!! Html::style("template/assets/css/bootstrap_limitless.min.css") !!}
    {!! Html::style("template/assets/css/layout.min.css")!!}
    {!! Html::style("template/assets/css/components.min.css") !!}
    {!! Html::style("template/assets/css/colors.min.css") !!}
    {!! Html::style('template/plugins/flatpickr/flatpickr.min.css') !!}
    {!! Html::style('template/plugins/clockpicker/clockpicker.css') !!}
    {!! Html::style('template/plugins/fancybox-master/dist/jquery.fancybox.min.css') !!} 
    {!! Html::style("css/font.css") !!}
    {!! Html::style("css/style.css") !!}
    {!! Html::script("template/assets/js/main/jquery.min.js") !!}
    {!! Html::script("template/assets/js/main/bootstrap.bundle.min.js") !!}
    {!! Html::script("template/assets/js/plugins/loaders/blockui.min.js") !!}
    {!! Html::script("template/assets/js/plugins/ui/ripple.min.js") !!}
    {!! Html::script("template/assets/js/plugins/notifications/sweet_alert.min.js")!!}
    {!! Html::script("template/assets/js/plugins/ui/perfect_scrollbar.min.js")!!}
    {!! Html::script("template/assets/js/plugins/forms/styling/uniform.min.js")!!}
    {!! Html::script("template/assets/js/plugins/forms/selects/select2.min.js")!!}
    {!! Html::script("template/assets/js/plugins/forms/selects/bootstrap_multiselect.js")!!}
    {!! Html::script("template/assets/js/plugins/tables/datatables/datatables.min.js")!!}
    {!! Html::script('template/plugins/formvalidation/dist/js/FormValidation.min.js')    !!}
    {!! Html::script('template/plugins/formvalidation/dist/js/plugins/Bootstrap.min.js') !!}
    {!! Html::script('template/plugins/flatpickr/flatpickr.js') !!}
    {!! Html::script('template/plugins/flatpickr/th.js') !!}
    {!! Html::script('template/plugins/clockpicker/clockpicker.js') !!}
    {!! Html::script('template/plugins/fancybox-master/dist/jquery.fancybox.min.js') !!} 
    {!! Html::script("template/assets/js/app.js") !!}
</head>
<body>
    <div class="page-content">
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-body border-top-info">
                        <h6 class="mb-3 font-weight-semibold font f-22">
                            รายละเอียดการชำระเงิน
                        </h6>
                        <div class="card">
                            <div class="card-body text-center">
                                <h6 class="font-weight-semibold mb-0 font f-22"> <strong> หัวข้อการักษา </strong> </h6>
                                <span class="d-block text-muted font f-20"> {{ $payment->treatment->title }} </span>
                            </div>
                        </div>
                        <div class="card card-body bg-light mb-0">
                            {{-- Dentist --}}
                            <dl class="row mb-0">
                                <dt class="col col-sm-1 font text-right">
                                    <strong> หมอ </strong>
                                </dt>
                                <dd class="col-sm-10">
                                    {{ $payment->treatment->dentist->name }}
                                </dd>
                            </dl>
                            {{-- Member --}}
                            <dl class="row mb-0">
                                <dt class="col col-sm-1 font text-right">
                                    <strong> ผู้รับบริการ </strong>
                                </dt>
                                <dd class="col-sm-3">
                                    {{ $payment->treatment->member->name }}
                                </dd>
                            </dl>
                            <hr>
                            {{-- Fees --}}
                            <dl class="row mb-0">
                                <dt class="col col-sm-1 font text-right">
                                    <strong> รายการที่รักษา </strong>
                                </dt>
                                <dd class="col-sm-10 font">
                                    <div class="row">
                                        @foreach ($payment->treatment->details_treatment as $item)
                                        <div class="col-md-5">
                                            {{ $item->name }}
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <b class="mr-3"> ราคา </b>
                                        </div>
                                        <div class="col-md-1 text-left">
                                            {{ number_format($item->price) }}
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <b class="ml-3"> บาท </b>
                                        </div>
                                        @endforeach
                                    </div>
                                </dd>
                            </dl>
                            @isset($payment->treatment->details_medicine[0])
                            <hr>
                            {{-- Medicines --}}
                            <dl class="row mb-0">
                                <dt class="col col-sm-1 font text-right">
                                    <strong> รายการยาที่เลือก </strong>
                                </dt>
                                <dd class="col-sm-10 font">
                                    <div class="row">
                                        @foreach ($payment->treatment->details_medicine as $item)
                                        <div class="col-md-5">
                                            {{ $item->name }}
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <b class="mr-3"> ราคา </b>
                                        </div>
                                        <div class="col-md-1 text-left">
                                            {{ number_format($item->price) }}
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <b class="ml-3"> บาท </b>
                                        </div>
                                        @endforeach
                                    </div>
                                </dd>
                            </dl>
                            @endisset
                            <hr>
                            {{-- Total --}}
                            <dl class="row mb-0">
                                <dt class="col col-sm-1 font text-center">
                                    <strong> รวมทั้งสิ้น </strong>
                                </dt>
                                <dd class="col-sm-3">
                                    {{ number_format($payment->treatment->total) }}
                                    <b class="ml-3 font"> บาท </b>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>