<!DOCTYPE html>
<html>
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <title> ใบเสร็จรับเงิน </title>
      <style>
        body { font-family: "THSarabunNew" }

        .absolute {
            position: absolute;
            text-align: right;
            vertical-align: middle;
        }

        table.separate {
            border-collapse: separate;
            width: 100%;
        }

        table.collapse {
            border-collapse: collapse;
            border: 1px solid black;
        }

        table.collapse td {
            padding-bottom : 10px;
        }

        table.collapse th {
            border: 1px solid black
        }

        .full-width {
            width: 100%;
        }

        th {
            text-align: center;
            vertical-align: middle;
        }

        td,th {
            vertical-align: middle;
        }

        .bold {
            font-weight : bold;
        }

        .border {
            border-left: 1px solid black;
        }

        .center {
            text-align: center;
        }

        .right {
            text-align: right;
        }

        .left {
            text-align: left;
        }

        .m_left {
            margin-left : 10px
        }

        .m_right {
            margin-right : 10px
        }

        .f_right {
            float : right;
        }

        .f_left {
            float : left;
        }
      </style>
  </head>
  <body marginwidth="0" marginheight="0">

    <h1>ใบเสร็จรับเงิน (Receipt) <span class="f_right"> คลินิกทันตกรรมอารมณ์ดี </span> </h1>
    <h3>
      <span class="m_right"> เล่มที่ {{ sprintf( "%00d", $payment->id ) }}  </span> <span class="f_right">  โทรศัพท์ 053-299558 เบอร์โทร 093-2781154 </span>
    </h3>
    <h3>
      <span class="m_right"> เลขที่ {{ sprintf( "%04d", $payment->id ) }}  </span> <span class="f_right">  ถนน โชตนา อ.แม่ริม จ.เชียงใหม่ 50180 </span>
    </h3>
    <hr>
    <br>

    <table class="separate">
      <tbody>
        <tr>
          <td width="15%"> <span style="font-weight:bold;font-size:20px"> หมอ </span> </td>
          <td> <span style="font-size:20px;"> {{ $payment->treatment->dentist->name }} </span> </td>
          <td width="20%"> <span style="font-weight:bold;font-size:20px"> วัน/เวลา ตรวจรักษา </span> </td>
          <td> <span style="font-size:20px;"> {{ date_thai($payment->created_at, TRUE) }} </span> </td>
        </tr>
        <tr>
          <td width="15%"> <span style="font-weight:bold;font-size:20px"> ผู้รับบริการ </span> </td>
          <td> <span style="font-size:20px;"> {{ $payment->treatment->member->name }} </span> </td>
          <td width="20%"> <span style="font-weight:bold;font-size:20px"> วัน/เวลา ชำระเงิน  </span> </td>
          <td> <span style="font-size:20px;"> {{ date_thai($payment->updated_at, TRUE) }} </span> </td>
        </tr>
      </tbody>
    </table>

    <br>
    <hr>


    <table class="separate">
      <tbody>
        <tr>
          <td class="center"> <span style="font-size:20px;"> {{ $payment->treatment->title }}  </span> </td>
        </tr>
      </tbody>
    </table>

    <br>


    <h3 style="margin-top:-10px;"> รายการรักษา </h3>
    <table class="collapse full-width">
      <thead>
        <tr>
          <th width="80%"> รายการ </th>
          <th width="20%"> ราคา  </th>
        </tr>
      </thead>
      <tbody>
        @foreach ($payment->treatment->details_treatment as $item)
        <tr>
            <td class="border">
                <span class="m_left"> {{ $item->name }} </span>
            </td>
            <td class="border center">
                <span class="m_left"> {{ number_format($item->price) }} </span>
            </td>
        </tr>
        @endforeach
      </tbody>
    </table>

    @isset($payment->treatment->details_medicine[0])
    <h3> รายการยา </h3>
    <table class="collapse full-width">
      <thead>
        <tr>
          <th width="80%"> รายการ </th>
          <th width="20%"> ราคา  </th>
        </tr>
      </thead>
      <tbody>
        @foreach ($payment->treatment->details_medicine as $item)
        <tr>
            <td class="border">
                <span class="m_left"> {{ $item->name }} </span>
            </td>
            <td class="border center">
                <span class="m_left"> {{ number_format($item->price) }} </span>
            </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    @endisset

    <br>
    <p class="f_right bold" style="font-size: 20px"> รวมเงินทั้งสิ้น  {{ number_format($payment->treatment->total, 2,'.', ',') }} บาท ({{ Numberthai($payment->treatment->total) }}) </p>
    <br>
    <br>
    <br>
    <hr>
    <hr>

  </body>
</html>
