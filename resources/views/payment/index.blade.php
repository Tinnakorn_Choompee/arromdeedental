@extends('layouts.app')
@section('title', 'Payment | การชำระเงิน')
@section('content')
@include('layouts.template.header',['icon'=> 'icon-cash  f-18', 'name' => 'การชำระเงิน'])
<div class="content">
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title font"> ข้อมูลการชำระเงิน </h5>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered table-striped datatable-treatments">
                    <thead>
                        <tr class="bg-slate-600 text-center">
                            <th class="text-center">ลำดับ</th>
                            <th>หัวข้อการรักษา</th>
                            <th>หมอ</th>
                            <th>ผู้รับบริการ</th>
                            <th>วันเวลาที่ตรวจรักษา</th>
                            <th>รวมค่าใช้จ่าย</th>
                            <th>สถานะการจ่ายเงิน</th>
                            <th>วันเวลาที่ชำระเงิน</th>
                            <th>เจ้าหน้าที่อนุมัติ</th>
                            <th>กระทำ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($payments as $k => $rs)
                        <tr>
                            <td class="text-center"> {{ ++$k }} </td>
                            <td class="text-left">
                                <a data-fancybox data-type="iframe" data-src="{{ route('payments.show', $rs->id) }}" href="javascript:;">
                                    {{ $rs->treatment->title }}
                                </a>
                            </td>
                            <td class="text-center"> {{ $rs->treatment->dentist->name  }} </td>
                            <td class="text-center"> {{ $rs->treatment->member->name   }} </td>
                            <td class="text-center"> {{ date_thai($rs->created_at, TRUE) }} </td>
                            <td class="text-center"> {{ number_format($rs->treatment->total, 0) }} </td>
                            <td class="text-center">
                                @switch($rs->status)
                                    @case('proceed')
                                    <span class="badge badge-flat border-warning text-warning-600 font f-16"> กำลังดำเนินการ </span>
                                    @break
                                    @case('success')
                                    <span class="badge badge-flat border-success text-success-600 font f-16"> จ่ายแล้ว </span>
                                    @break
                                @endswitch
                            </td>
                            <td class="text-center">
                                @if($rs->status == "success")
                                {{ date_thai($rs->updated_at, TRUE) }}
                                @endif
                            </td>
                            <td class="text-center">
                                @if($rs->status == "success")
                                    {{ $rs->authority->name }}
                                @endif
                            </td>
                            <td class="center">
                                @switch($rs->status)
                                    @case("proceed")
                                    <button class="btn btn-outline-success font f-18 border-2 btn-approve" data-id="{{ $rs->id }}"> <i class="icon-point-right mr-2"></i> อนุมัติ </button>
                                    {{ Form::open(['method' => 'PATCH', 'route' => ['payments.update', $rs->id], 'id'=>'approve-'.$rs->id])}} {{ Form::close() }}
                                    @break
                                    @case("success")
                                    <a target="_blank" class="btn btn-outline-info font f-18 border-2" href="{{ route('payments.print', $rs->id) }}"> <i class="fa fa-print"></i> </a>
                                    @break
                                @endswitch
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <br>
            </div>
        </div>
    </div>
</div>
@endsection

@if (session('approve'))
    <script>
        swal({ title: 'Success!', text: 'ทำการชำระเงินเรียบร้อยแล้ว', type: 'success'});
    </script>
@endif

@push('scripts')
    <script>
    $('.btn-approve').on('click',function(){
        const id = $(this).data('id');
        swal({
            title: 'Are you sure?',
            text: "ต้องการที่จะยืนยัน การชำระเงิน นี้ใช่หรือไม่ !!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'ยืนยัน',
            cancelButtonText: 'ยกเลิก',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function (result) {
            if (result.value) {
                $( "#approve-"+id ).submit();
            }
        });
    });
    </script>
@endpush

