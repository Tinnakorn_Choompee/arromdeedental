@extends('layouts.app')
@section('title', 'Equipment | อุปกรณ์')
@section('content')
@include('layouts.template.header',['icon'=> 'fa-wrench f-18', 'name' => 'อุปกรณ์'])
<div class="content">
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title font"> ข้อมูลอุปกรณ์ </h5>
            @can('equipment-create')
            <a href="{{ route('equipment.create') }}" class="btn alpha-blue text-blue-800 border-blue-600 font" style="font-size:18px">
                <i class="icon-folder-plus3 mr-2"></i> ข้อมูลอุปกรณ์
            </a>
            @endcan
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered table-striped datatable-medicines">
                    <thead>
                        <tr class="bg-slate-600 text-center">
                            <th class="text-center">ลำดับ</th>
                            <th>รหัส</th>
                            <th>ชื่อ</th>
                            <th>คุณสมบัติ</th>
                            <th>จำนวน</th>
                            <th>หน่วย</th>
                            @can('stock_equipment-create')
                            <th>สต๊อก</th>
                            @endcan
                            <th>กระทำ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($equipment as $k => $rs)
                        <tr>
                            <td class="text-center"> {{ ++$k }}</td>
                            <td class="text-center">{{ $rs->code }}</td>
                            <td>{{ $rs->name }}</td>
                            <td>{{ $rs->properties }}</td>
                            <td class="text-center">
                                @if($rs->amount < 5)
                                    <span class="text-danger font-weight-bold">  {{ $rs->amount }} </span>
                                @else
                                    <span class="text-default font-weight-bold"> {{ $rs->amount }} </span>
                                @endif
                            </td>
                            <td class="text-center">{{ $rs->type }}</td>
                            @can('stock_equipment-create')
                            <td class="center">
                                @if($rs->amount > 5)
                                    <button class="btn btn-success btn-stock" 
                                        data-type      = "equipment"
                                        data-name      = "{{ $rs->name }}" 
                                        data-id        = "{{ $rs->id }}"
                                        data-placement = "bottom"
                                        data-popup     = "tooltip"
                                        title          = "เพิ่มสต๊อกอุปกรณ์">
                                        <i class="fa fa-cube"></i>
                                    </button>  
                                @else
                                    <button class="btn btn-warning btn-stock" 
                                        data-type      = "equipment"
                                        data-name      = "{{ $rs->name }}" 
                                        data-id        = "{{ $rs->id }}"
                                        data-placement = "bottom"
                                        data-popup     = "tooltip"
                                        title          = "เพิ่มสต๊อกอุปกรณ์">
                                        <i class="fa fa-cube"></i>
                                    </button>  
                                @endif
                            </td>   
                            @endcan 
                            <td class="center">
                                @include('layouts.pattern.action', ['name'=> 'equipment', 'id' => $rs->id])
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <br>
            </div>
        </div>
    </div>
</div>
@endsection
