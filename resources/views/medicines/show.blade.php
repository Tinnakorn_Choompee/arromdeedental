@extends('layouts.app')
@section('title', 'ข้อมูลยา')
@section('content')
@include('layouts.template.header', ['icon'=> 'fa fa-user-md f-18', 'name' => 'ข้อมูลยา'])
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-body border-top-info">
                <h6 class="mb-3 font-weight-semibold font f-22">
                    รายละเอียดยา
                </h6>
                <div class="card">
                    <div class="card-body text-center">
                        <h6 class="font-weight-semibold mb-0 font f-22"> <strong> ยา </strong> </h6>
                        <span class="d-block text-muted font f-20"> {{ $medicine->name }} </span>
                    </div>
                </div>
                <div class="card card-body bg-light mb-0">
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> รหัสยา </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ $medicine->code }}
                        </dd>
                    </dl>
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> คุณสมบัติ </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ $medicine->properties }}
                        </dd>
                    </dl>
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> จำนวน </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ $medicine->amount }}

                            {{ $medicine->type }}
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
