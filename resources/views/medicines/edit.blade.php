@extends('layouts.app')
@section('title', 'แก้ไขยา')
@section('content')
@include('layouts.template.header', ['icon'=>'fa-medkit f-18', 'name' => 'แก้ไขยา'])
<div class="content">
    {{ Form::model($medicine,['method'=>'PATCH', 'route'=> ['medicines.update', $medicine->id], 'id' => 'FormValidation']) }}
    @include('layouts.pattern.errors', ['errors' => $errors])
    <div class="card">
        <div class="card-body">
            <fieldset class="mb-3">
                <legend class="text-uppercase font-weight-bold font">
                    รายละเอียดยา
                </legend>
                <div class="form-group row">
                    {{ Form::label('code', 'รหัสยา', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::text('code', null, ['class' => 'form-control'] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('name', 'ชื่อยา', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::text('name', null, ['class' => 'form-control'] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('properties', 'คุณสมบัติ', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::textarea('properties', null, ['rows' => 2, 'class' => 'form-control'] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('type', 'ประเภทยา', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::select('type', $type,  NULL, ['class' => 'form-control form-control-uniform', 'placeholder'=>'-- เลือกประเภทยา --'] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('amount', 'จำนวน', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::number('amount', null, ['class' => 'form-control phone', 'maxlength' => 10, 'readonly'] ) }}
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="text-center">
                <button type="submit" class="btn btn-primary font"> บันทึก <i class="icon-paperplane ml-2"></i></button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>
@endsection
@push('scripts')
<script>
    document.addEventListener('DOMContentLoaded', function(e) {
        const form = document.getElementById('FormValidation');
        FormValidation.formValidation(form, {
            fields: {
                code: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณากรอกรหัสยา'
                        }
                    }
                },
                name: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณากรอกชื่อยา'
                        }
                    }
                },
                properties: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณากรอกคุณสมบัติยา'
                        }
                    }
                },
                amount: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณากรอกจำนวนยา'
                        },
                        numeric: {
                            message: 'The value is not a number',
                            thousandsSeparator: '',
                            decimalSeparator: '.'
                        }
                    }
                },
                type: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณาเลือกประเภทยา'
                        }
                    }
                }
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap(),
                submitButton: new FormValidation.plugins.SubmitButton(),
                defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            },
        });
    });
</script>
@endpush
