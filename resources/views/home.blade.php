@extends('layouts.app')
@section('title', 'Home | หน้าหลัก')
@section('content')
@include('layouts.template.header', ['icon'=> 'icon-home2', 'name' => 'หน้าหลัก'])
<div class="content">
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title font"> ผู้ใช้งาน และ ผู้รับริการ </h6>
                </div>
                <div class="card-body px-2">
                    <div class="row">
                        <div class="col-sm-4 col-6">
                            <div class="d-flex align-items-center justify-content-center mb-2">
                                <a href="{{ route('authority.index') }}"
                                    class="btn bg-transparent border-warning-400 text-warning-400 rounded-round border-2 btn-icon mr-3">
                                    <i class="icon-users"></i>
                                </a>
                                <div>
                                    <div class="font-weight-semibold font f-20"> เจ้าหน้าที่ </div>
                                    <span class="text-muted f-16"> {{ users_count("authority") }} </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-6">
                            <div class="d-flex align-items-center justify-content-center mb-2">
                                <a href="{{ route('dentist.index') }}"
                                    class="btn bg-transparent border-teal text-teal rounded-round border-2 btn-icon mr-3">
                                    <i class="fa fa-user-md f-18"></i>
                                </a>
                                <div>
                                    <div class="font-weight-semibold font f-20"> หมอ </div>
                                    <span class="text-muted f-16"> {{ users_count("dentist") }} </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-6">
                            <div class="d-flex align-items-center justify-content-center mb-2">
                                <a href="{{ route('member.index') }}"
                                    class="btn bg-transparent border-indigo-400 text-indigo-400 rounded-round border-2 btn-icon mr-3">
                                    <i class="icon-users4"></i>
                                </a>
                                <div>
                                    <div class="font-weight-semibold font f-20"> ผู้รับบริการ </div>
                                    <span class="text-muted f-16"> {{ users_count("member") }} </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title font"> รายงานการรักษา ประจำปี {{ Carbon::now()->year + 543 }}</h6>
                </div>
                <div class="card-body px-2">
                    <div class="row">
                        <div class="width_chart">
                            <canvas id="ChartTreatment" height="100"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
{!! Html::style('template/plugins/chart/Chart.min.css') !!}
@endpush

@push('scripts')
{!! Html::script('template/plugins/chart/Chart.min.js') !!}
{!! Html::script('template/plugins/chart/config.js') !!}
<script>
var treatment      = document.getElementById('ChartTreatment').getContext('2d');
var ChartTreatment = new Chart(treatment, {
    type: 'bar',
    data: {
        labels: short,
        datasets: [{
            data: [@foreach(treatment_month() as $rs) {{ $rs }}, @endforeach],
            backgroundColor: color,
            borderColor: border,
            borderWidth: 1
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    stepSize: 1,
                    max : {{ max(treatment_month()) + 2 }}
                }
            }]
        }
    }
});
</script>
@endpush
