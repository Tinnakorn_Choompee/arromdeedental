@extends('layouts.app')
@section('title', 'Appointments | การนัดหมาย')
@section('content')
@include('layouts.template.header',['icon'=> 'icon-copy2 f-18', 'name' => 'การนัดหมาย'])
<div class="content">
    <div class="card">
        <div class="card-header">
            <div class="row">
                @unlessrole('dentist')
                    <div class="col-md-4">
                        <h5 class="card-title font">
                            <div class="form-group row">
                                <label class="col-form-label col-md-1">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-calendar22"></i></span>
                                    </span>
                                </label>
                                <div class="col-md-6">
                                    <form action="{{ route('appointments.index') }}" method="GET" id="form-date">
                                        <input type="text" class="form-control date" placeholder="เลือกวันนัด" name="date" id="date_range" required value="{{ $date_range ?  $date_range : NULL }}">
                                    </form>
                                </div>
                                <label class="col-form-label col-md-5">
                                    <button id="search" class="btn btn-primary" data-popup="tooltip" title="ค้นหา" data-placement="bottom" type="submit" style="margin-top:5px;"><i class="icon-search4"></i></button>
                                    <a href="{{ route('appointments.index') }}" class="btn bg-teal" data-popup="tooltip" title="รีเซ็ต" data-placement="bottom" style="margin-top:5px;"> <i class="icon-reset"></i> </a>
                                </label>
                            </div>
                        </h5>
                    </div>
                    <div class="col-md-6 text-center">
                        <div class="header-elements">
                            <span class="text-primary font f-18"> ** กรณีผู้รับบริการมาตามนัดหมาย และ ทำการตรวจเสร็จสิ้น ให้กดปุ่ม <b> รายละเอียด </b> เพื่อยืนยันการนัดหมายสำเร็จ </span>
                        </div>
                    </div>
                @else
                <div class="col-md-4">
                    <h2 class="font"> การนัดหมาย ของ วันที่ {{ date_thai(Carbon::now()) }}</h2>
                </div>
                @endunlessrole

                <div class="col-md-2 text-right">
                    @can('appointments-create')
                    <a href="{{ route('appointments.create') }}" class="btn alpha-blue text-blue-800 border-blue-600 font"
                        style="font-size:18px">
                        <i class="icon-file-plus mr-2"></i> เพิ่มการนัดหมาย
                    </a>
                    @endcan
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered table-striped datatable-appointments">
                    <thead>
                        <tr class="bg-slate-600 text-center">
                            <th class="text-center">ลำดับ</th>
                            <th>หมอ </th>
                            <th>ผู้รับบริการ </th>
                            <th>วัน/เวลา ที่นัด </th>
                            <th>รายละเอียดการนัด </th>
                            <th>เจ้าหน้าที่ผู้นัด </th>
                            <th>สถานะ</th>
                            <th>กระทำ </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($appointments as $k => $rs)
                        <tr>
                            <td class="text-center"> {{ ++$k }}</td>
                            <td>{{ $rs->dentist->name   }}</td>
                            <td>{{ $rs->member->name    }}</td>
                            <td class="center">{{ date_thai($rs->datetime, TRUE) }}</td>
                            <td>{{ $rs->detail }}</td>
                            <td>{{ $rs->authority->name }}</td>
                            <td class="center">
                                @switch($rs->status)
                                @case("proceed")
                                <span class="badge badge-flat border-warning text-warning-600 font f-20"> กำลังนัดหมาย
                                </span>
                                @break
                                @case("success")
                                <span class="badge badge-flat border-success text-success-600 font f-20"> นัดหมายสำเร็จ
                                </span>
                                @break
                                @endswitch
                            </td>
                            <td class="center">
                                <div class="list-icons">
                                    @switch($rs->status)
                                    @case('proceed')
                                    <a href="{{ route('appointments.show', $rs->id) }}" data-popup="tooltip"
                                        title="รายละเอียด" data-placement="bottom" class="btn btn-primary"
                                        title="ดูข้อมูล">
                                        <i class="icon-eye"></i>
                                    </a>
                                    @can('appointments-edit')
                                    <a class="btn btn-warning btn-edit" href="{{ route('appointments.edit',$rs->id) }}"
                                        data-popup="tooltip" title="แก้ไข" data-placement="bottom">
                                        <i class="icon-pencil7"></i>
                                    </a>
                                    @endcan
                                    @can('appointments-delete')
                                    <button class="btn btn-danger  btn-del" data-id="{{ $rs->id }}" data-popup="tooltip"
                                        title="ลบ" data-placement="bottom">
                                        <i class="icon-trash"></i>
                                    </button>
                                    {{ Form::open(['method' => 'DELETE', 'route' => ['appointments.destroy', $rs->id], 'id'=>'delete-'.$rs->id])}}
                                    {{ Form::close() }}
                                    @endcan
                                    @break
                                    @case('success')
                                    <a href="{{ route('appointments.show', $rs->id) }}" data-popup="tooltip"
                                        title="รายละเอียด" data-placement="bottom" class="btn btn-primary"
                                        title="ดูข้อมูล">
                                        <i class="icon-eye"></i>
                                    </a>
                                    @can('appointments-delete')
                                    <button class="btn btn-danger  btn-del" data-id="{{ $rs->id }}" data-popup="tooltip"
                                        title="ลบ" data-placement="bottom">
                                        <i class="icon-trash"></i>
                                    </button>
                                    {{ Form::open(['method' => 'DELETE', 'route' => ['appointments.destroy', $rs->id], 'id'=>'delete-'.$rs->id])}}
                                    {{ Form::close() }}
                                    @endcan
                                    @break
                                    @endswitch
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <br>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
    <style>
        .datatable-header {
            padding: 0.1rem 1.25rem 0;
        }
        .input-group-prepend {
            margin-top:5px;
        }
        .flatpickr-input[readonly] {
            margin-top: 10px !important;
        }
        .form-control[readonly] {
            font-size: 20px;
            text-align: center;
        }
        #date_range::-webkit-input-placeholder {
            font-size: 18px;
            text-indent: 0.5em;
        }
        .date_range_required::-webkit-input-placeholder {
            color: red;
        }
    </style>
@endpush

@push('scripts')
    <script>
    $('.date').flatpickr({
        mode: "range",
        locale: "th",
        minDate : "today",
    })
    $('#search').on('click', function() {
        console.log($('#date_range').val());
        if ($('#date_range').val() != '') { $('#form-date').submit() }
        $('#date_range').attr("placeholder", "กรุณาเลือกวันนัด");
        $('#date_range').addClass('date_range_required');
    })
    </script>
@endpush
