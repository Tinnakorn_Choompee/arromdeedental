@extends('layouts.app')
@section('title', 'ข้อมูลการนัดหมาย')
@section('content')
@include('layouts.template.header', ['icon'=> 'fa fa-user-md f-18', 'name' => 'ข้อมูลการนัดหมาย'])
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-body border-top-info">
                <h6 class="mb-3 font-weight-semibold font f-22">
                    รายละเอียดการนัดหมาย
                </h6>
                <div class="card">
                    <div class="card-body text-center">
                        <h6 class="font-weight-semibold mb-0 font f-22"> <strong> การนัดหมาย </strong> </h6>
                        <span class="d-block text-muted font f-20"> {{ date_thai($appointment->datetime, TRUE) }}
                        </span>
                    </div>
                </div>
                <div class="card card-body bg-light mb-3">
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> หมอ </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ $appointment->dentist->name }}
                        </dd>
                    </dl>
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> ผู้รับบริการ </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ $appointment->member->name }}
                        </dd>
                    </dl>
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> รายละเอียดการนัด </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ $appointment->detail }}
                        </dd>
                    </dl>
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> เจ้าหน้าที่ผู้นัด </strong>
                        </dt>
                        <dd class="col-sm-3">
                            {{ $appointment->authority->name }}
                        </dd>
                    </dl>
                    <dl class="row justify-content-md-center mb-0">
                        <dt class="col col-sm-3 font text-right">
                            <strong> สถานะ </strong>
                        </dt>
                        <dd class="col-sm-3">
                            @switch($appointment->status)
                            @case("proceed")
                            <span class="badge badge-flat border-warning text-warning-600 font f-20"> กำลังดำเนินการ
                            </span>
                            @break
                            @case("success")
                            <span class="badge badge-flat border-success text-success-600 font f-20"> ดำเนินการสำเร็จ
                            </span>
                            @break
                            @endswitch
                        </dd>
                    </dl>
                </div>

                @role("admin")
                @if($appointment->status == "proceed")
                <div class="card">
                    <div class="card-body">
                        <div class="text-center">
                            <button class="btn btn-success font f-20 success" data-id="{{ $appointment->id }}"> ยืนยันการนัดหมายสำเร็จ <i class="icon-point-up ml-2"></i></button>
                        </div>
                    </div>
                </div>
                @endif
                @endrole

                @role("dentist")
                @if($appointment->status == "proceed")
                <div class="card">
                    <div class="card-body">
                        <div class="text-center">
                            <a href="{{ route('treatments.create',['appointment'=> $appointment->id]) }}" class="btn btn-primary font f-20" data-id="{{ $appointment->id }}"> กรอกรายละเอียดการรักษา <i class="icon-point-up ml-2"></i></a>
                        </div>
                    </div>
                </div>
                @endif
                @endrole

            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.success').on('click',function(){
            const id = $(this).data('id');
            swal({
                title: 'Are you sure?',
                text: "ยืนยันการนัดหมายสำเร็จ !!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'ยืนยัน',
                cancelButtonText: 'ยกเลิก',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        type: "PATCH",
                        url: "/success/appointment",
                        data: { id : id },
                        success: function(rs){
                            location.reload();
                        }
                    });
                }
            });
        });
    </script>
@endpush
