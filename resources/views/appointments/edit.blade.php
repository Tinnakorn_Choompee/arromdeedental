@extends('layouts.app')
@section('title', 'แก้ไขทันตแพทย์')
@section('content')
@include('layouts.template.header', ['icon'=>'fa fa-user-md f-18', 'name' => 'แก้ไขทันตแพทย์'])
<div class="content">
    {{ Form::model($appointment ,['method'=>'PATCH', 'route'=> ['appointments.update', $appointment->id], 'id' => 'FormValidation']) }}
    @include('layouts.pattern.errors', ['errors' => $errors])
    <div class="card">
        <div class="card-body">
            <fieldset class="mb-3">
                <legend class="text-uppercase font-weight-bold font">
                    รายละเอียดการนัดหมาย
                </legend>
                <div class="form-group row">
                    {{ Form::label('name', 'ทันตแพทย์', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::select('dentist_id', $dentist ,  NULL, ['class' => 'form-control select-search', 'placeholder'=>'-- เลือกทันตแพทย์ --'] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('name', 'ผู้รับบริการ', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::select('member_id', $member ,  NULL, ['class' => 'form-control select-search', 'placeholder'=>'-- เลือกผู้รับบริการ --'] ) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('detail', 'รายละเอียดการนัด', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::textarea('detail', null, ['rows' => 3, 'class' => 'form-control'] ) }}
                    </div>
                </div>
                <div class="form-group row ">
                    {{ Form::label('open_date', 'วัน/เวลา ที่นัด', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-5">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar22"></i></span>
                            </span>
                            <input type="text" class="form-control date" placeholder="วัน" name="date" autocomplete="off" value="{{ Carbon::parse($appointment->datetime)->format('Y-m-d') }}">
                        </div>
                    </div>
                    <div class="col-lg-5">
                       <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-alarm"></i></span>
                            </span>
                            <input type="text" class="form-control time" placeholder="เวลา" data-autoclose="true" name="time" autocomplete="off" value="{{ Carbon::parse($appointment->datetime)->format('H:i') }}">
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="text-center">
                <button type="submit" class="btn btn-primary font"> บันทึก <i class="icon-paperplane ml-2"></i></button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>
@endsection
@push('scripts')
<script>
    document.addEventListener('DOMContentLoaded', function(e) {
        const form = document.getElementById('FormValidation');
        FormValidation.formValidation(form, {
            fields: {
                dentist_id: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณาเลือกทันตแพทย์'
                        }
                    }
                },
                member_id: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณาเลือกผู้รับบริการ'
                        }
                    }
                },
                detail: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณากรอกรายละเอียดการนัดหมาย'
                        }
                    }
                },
                date: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณาเลือกวัน'
                        }
                    }
                },
                time: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณาเลือกเวลา'
                        }
                    }
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap(),
                submitButton: new FormValidation.plugins.SubmitButton(),
                defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            },
        });
    });
    $('.time').clockpicker();
    $('.date').flatpickr({locale: "th", minDate : "today"});
</script>
@endpush
