@extends('layouts.app')
@section('title', 'Stock Equipment | การสต๊อกอุปกรณ์')
@section('content')
@include('layouts.template.header',['icon'=> 'icon-hammer-wrench', 'name' => 'การสต๊อกอุปกรณ์'])
<div class="content">
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title font"> ข้อมูลการสต๊อกอุปกรณ์ </h5>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                {!! Form::open(['route' => 'stock_equipment.delete', 'method' => 'DELETE', 'id'=>'stock_equipment']) !!}
                <table class="table table-hover table-bordered table-striped datatable-stock">
                    <thead>
                        <tr class="bg-slate-600 text-center">
                            @can('stock_equipment-delete')
                            <th class="center"> 
                                <input type="checkbox" data-toggle="toggle" data-size="xs" data-onstyle="danger" id="toggle-all">
                            </th>
                            @endcan
                            <th class="text-center">ลำดับ</th>
                            <th>รหัส</th>
                            <th>ชื่อ</th>
                            <th>จำนวน</th>
                            <th>คงเหลือ</th>
                            <th>วัน/เวลา</th>
                            <th>เจ้าหน้าที่</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($equipment as $k => $rs)
                        <tr class="center">
                            @can('stock_equipment-delete')
                            <td class="center">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input-styled-danger check-all" name="id[]" value="{{ $rs->id }}">
                                </label> 
                            </td>
                            @endcan
                           <td>{{ ++$k }}</td>
                           <td>{{ $rs->equipment->code  }}</td> 
                           <td class="text-left">{{ $rs->equipment->name  }}</td> 
                           <td>{{ $rs->qty }}</td> 
                           <td><b>{{ $rs->equipment->amount }}</b></td>
                           <td>{{ date_thai($rs->created_at, TRUE) }}</td> 
                           <td>{{ $rs->authority->name }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! Form::close() !!}
                <br>
                @can('stock_equipment-delete')
                <button class="btn btn-danger btn-all font" disabled style="margin-top:-9%"> <i class="fa fa-trash"></i> ลบข้อมูลที่เลือก </button>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
{!! Html::style('template/plugins/toggle/bootstrap4-toggle.min.css') !!}
@endpush
@push('scripts')
{!! Html::script('template/plugins/toggle/bootstrap4-toggle.min.js') !!}
<script>
    $('.btn-all').on('click',function(){
        swal({
            title: 'Are you sure?',
            text: "ต้องการที่จะลบ ข้อมูล นี้ใช่หรือไม่ !!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'ลบ',
            cancelButtonText: 'ยกเลิก',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        }).then(function (result) {
            if (result.value) {
                $("#stock_equipment").submit();
            }
        });
    });
</script>
@endpush
