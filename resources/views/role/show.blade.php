@extends('layouts.app')
@section('title', 'ข้อมูลสิทธิ์การใช้งาน')
@section('content')
@include('layouts.template.header',['icon'=> 'icon-collaboration', 'name' => 'ข้อมูลสิทธิ์การใช้งาน'])
<div class="content">
    <div class="card">
        <div class="card-header">
            <legend class="font-size-sm font-weight-bold font f-20"> ข้อมูลสิทธิ์การใช้งาน <span class="f-20 float-right"> {{ $role->name }} </span> </legend>
        </div>
        <div class="card-body">
            <p class="text-muted">
                <div class="row">
                    <div class="col-md-6">
                        @if(!empty($rolePermissions))
                            @foreach($rolePermissions as $v)
                                {{ $v->name }} <hr>
                            @endforeach
                        @endif
                    </div>
                </div>
            </p>
            <hr>
        </div>
    </div>
</div>
@endsection
