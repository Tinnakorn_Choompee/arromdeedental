@extends('layouts.app')
@section('title', 'แก้ไขสิทธิ์ผู้ใช้งาน')
@section('content')
@include('layouts.template.header',['icon'=> 'icon-collaboration', 'name' => 'แก้ไขสิทธิ์ผู้ใช้งาน'])
<div class="content">
    <div class="card">
        <div class="card-header header-elements-inline">
            <legend class="text-uppercase font-size-sm font-weight-bold font f-20"> รายละเอียดสิทธิ์การใช้งาน </legend>
        </div>
        <div class="card-body">
            {!! Form::model($role, ['route' => ['role.update', $role->id], 'method' => 'PATCH']) !!}
                <fieldset class="mb-4">
                    <div class="form-group row">
                        {{ Form::label('name', 'สิทธิ์การใช้งาน', ['class'=>'col-form-label col-lg-2 font']) }}
                        <div class="col-lg-10">
                            {{ Form::text('name', NULL, ['class'=> 'form-control', 'readonly']) }}
                            @if ($errors->has('name'))
                                <span class="form-text text-danger font"> {{ $errors->first('name') }} </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 font"> การอนุญาติเข้าใช้งาน </label>
                        <div class="col-lg-10">
                            <div class="row">
                                <div class="col-md-12 mb-4">
                                    @foreach($permission as $value)
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                {{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, ['class' => 'form-check-input-styled-primary', 'data-fouc'])}}
                                                {!! $value->name !!}
                                            </label>
                                        </div>
                                        <hr>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary font"> บันทึก <i class="icon-paperplane ml-2"></i></button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    <!-- /form inputs -->
</div>
<!-- /content area -->
@endsection
@push('scripts')
    {!! Html::script("theme/global_assets/js/plugins/forms/styling/uniform.min.js") !!}
    <script>
    $('.form-check-input-styled-primary').uniform({
        wrapperClass: 'border-primary-600 text-primary-800'
    });
    </script>
@endpush

