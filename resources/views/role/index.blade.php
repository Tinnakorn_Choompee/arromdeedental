@extends('layouts.app')
@section('title', 'Role | สิทธิ์การใช้งาน')
@section('content')
@include('layouts.template.header',['icon'=> 'icon-collaboration', 'name' => 'สิทธิ์การใช้งาน'])
<div class="content">
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title font f-22"> สิทธิ์การใช้งาน </h5>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered table-striped">
                    <thead>
                        <tr class="bg-slate-600">
                            <th width="50%">ประเภทผู้ใช้งาน</th>
                            <th width="40%">กระทำ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($roles as $K => $rs)
                        <tr>
                            <td>{{ $rs->name }}</td>
                            <td>
                                <div class="list-icons">
                                    <a href="{{ route('role.show', $rs->id) }}" class="btn list-icons-item text-primary-600"><i class="icon-file-eye"></i></a>
                                    @can('role-edit')
                                    <a href="{{ route('role.edit', $rs->id) }}" class="btn list-icons-item text-warning-600"><i class="icon-pencil7"></i></a>
                                    @endcan
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
