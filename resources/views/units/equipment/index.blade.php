@extends('layouts.app')
@section('title', 'Type Equipment | ประเภทอุปกรณ์')
@section('content')
@include('layouts.template.header',['icon'=> 'icon-hammer-wrench', 'name' => 'ประเภทอุปกรณ์'])
<div class="content">
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title font"> ข้อมูลประเภทอุปกรณ์ </h5>
            @can('type_equipment-create')
            <a href="{{ route('type_equipment.create') }}" class="btn alpha-blue text-blue-800 border-blue-600 font" style="font-size:18px">
                <i class="icon-folder-plus3 mr-2"></i> เพิ่มประเภทอุปกรณ์
            </a>
            @endcan
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered table-striped datatable-type">
                    <thead>
                        <tr class="bg-slate-600 text-center">
                            <th class="text-center">ลำดับ</th>
                            <th>ชื่อ</th>
                            <th>กระทำ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($equipment as $k => $rs)
                        <tr>
                            <td class="text-center"> {{ ++$k }}      </td>
                            <td class="text-center"> {{ $rs->name }} </td>
                            <td class="center">
                                @include('layouts.pattern.action', ['name'=> 'type_equipment', 'id' => $rs->id])
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <br>
            </div>
        </div>
    </div>
</div>
@endsection
