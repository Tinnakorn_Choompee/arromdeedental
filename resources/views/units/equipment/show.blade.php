@extends('layouts.app')
@section('title', 'ข้อมูลประเภทอุปกรณ์')
@section('content')
@include('layouts.template.header', ['icon'=> 'icon-hammer-wrench', 'name' => 'ข้อมูลประเภทอุปกรณ์'])
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-body border-top-info">
                <h6 class="mb-3 font-weight-semibold font f-22">
                    รายละเอียดประเภทอุปกรณ์
                </h6>
                <div class="card">
                    <div class="card-body text-center">
                        <h6 class="font-weight-semibold mb-0 font f-22"> <strong> ประเภทอุปกรณ์ </strong> </h6>
                        <span class="d-block text-muted font f-20"> {{ $equipment->name }} </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
