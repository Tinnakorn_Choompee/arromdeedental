@extends('layouts.app')
@section('title', 'แก้ไขประเภทยา')
@section('content')
@include('layouts.template.header', ['icon'=>'icon-aid-kit', 'name' => 'เพิ่มประเภทยา'])
<div class="content">
    {{ Form::model($medicines, ['method'=>'PATCH', 'route'=> ['type_medicines.update', $medicines->id], 'id' => 'FormValidation']) }}
    @include('layouts.pattern.errors', ['errors' => $errors])
    <div class="card">
        <div class="card-body">
            <fieldset class="mb-3">
                <legend class="text-uppercase font-weight-bold font">
                    รายละเอียดประเภทยา
                </legend>
                <div class="form-group row">
                    {{ Form::label('name', 'ชื่อประเภทยา', ['class'=>'col-form-label col-lg-2 font']) }}
                    <div class="col-lg-10">
                        {{ Form::text('name', null, ['class' => 'form-control'] ) }}
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="text-center">
                <button type="submit" class="btn btn-primary font"> บันทึก <i class="icon-paperplane ml-2"></i></button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>
@endsection
@push('scripts')
<script>
    document.addEventListener('DOMContentLoaded', function(e) {
        const form = document.getElementById('FormValidation');
        FormValidation.formValidation(form, {
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณากรอกชื่อประเภทยา'
                        }
                    }
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap(),
                submitButton: new FormValidation.plugins.SubmitButton(),
                defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            },
        });
    });
</script>
@endpush
