<?php

namespace App\Http\Requests\Profile;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required|unique:users,name,'.Auth::user()->id,
            'username' => 'required|unique:users,username,'.Auth::user()->id,
            'email'    => 'required|email|unique:users,email,'.Auth::user()->id,
            'phone'    => 'required|unique:users,phone,'.Auth::user()->id,
            'password' => 'confirmed',
            'image'    => 'mimes:jpeg,png,jpg'
        ];
    }

    public function messages()
    {
        return [
            'name.unique'     => 'มีชื่อเจ้าหน้าที่ในระบบแล้ว',
            'username.unique' => 'มีชื่อผู้ใช้งานในระบบแล้ว',
            'email.unique'    => 'มีอีเมล์นี้ในระบบแล้ว',
            'phone.unique'    => 'มีเบอร์โทรนี้ในระบบแล้ว',
            'image.mimes'     => 'กรุณาเลือกไฟล์รูปภาพ'
        ];
    }
}
