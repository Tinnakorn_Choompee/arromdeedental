<?php

namespace App\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class Update extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(Request $request)
    {
        return [
            'name'     => 'required|unique:members,name,'.$request->id,
            'phone'    => 'required|numeric|unique:members,phone,'.$request->id,
            'image'    => 'mimes:jpeg,png,jpg'
        ];
    }

    public function messages()
    {
        return [
            'name.unique'     => 'มีชื่อผู้รับบริการในระบบแล้ว',
            'phone.unique'    => 'มีเบอร์โทรนี้ในระบบแล้ว',
            'image.mimes'     => 'กรุณาเลือกไฟล์รูปภาพ'
        ];
    }
}
