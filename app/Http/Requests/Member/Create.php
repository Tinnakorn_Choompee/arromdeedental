<?php

namespace App\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;

class Create extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'     => 'required|unique:members',
            'phone'    => 'required|unique:members|numeric',
            'image'    => 'mimes:jpeg,png,jpg'
        ];
    }

    public function messages()
    {
        return [
            'name.unique'     => 'มีชื่อผู้รับบริการในระบบแล้ว',
            'phone.unique'    => 'มีเบอร์โทรนี้ในระบบแล้ว',
            'image.mimes'     => 'กรุณาเลือกไฟล์รูปภาพ'
        ];
    }
}
