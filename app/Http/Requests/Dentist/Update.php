<?php

namespace App\Http\Requests\Dentist;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class Update extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(Request $request)
    {
        return [
            'name'     => 'required|unique:users,name,'.$request->id,
            'username' => 'required|unique:users,username,'.$request->id,
            'email'    => 'required|email|unique:users,email,'.$request->id,
            'phone'    => 'required|numeric|unique:users,phone,'.$request->id,
            'image'    => 'mimes:jpeg,png,jpg'
        ];
    }

    public function messages()
    {
        return [
            'name.unique'     => 'มีชื่อเจ้าหน้าที่ในระบบแล้ว',
            'username.unique' => 'มีชื่อผู้ใช้งานในระบบแล้ว',
            'email.unique'    => 'มีอีเมล์นี้ในระบบแล้ว',
            'phone.unique'    => 'มีเบอร์โทรนี้ในระบบแล้ว',
            'image.mimes'     => 'กรุณาเลือกไฟล์รูปภาพ'
        ];
    }
}
