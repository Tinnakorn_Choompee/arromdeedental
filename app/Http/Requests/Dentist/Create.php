<?php

namespace App\Http\Requests\Dentist;

use Illuminate\Foundation\Http\FormRequest;

class Create extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'     => 'required|unique:users',
            'username' => 'required|unique:users',
            'email'    => 'required|unique:users',
            'phone'    => 'required|unique:users|numeric',
            'image'    => 'mimes:jpeg,png,jpg'
        ];
    }

    public function messages()
    {
        return [
            'name.unique'     => 'มีชื่อเจ้าหน้าที่ในระบบแล้ว',
            'username.unique' => 'มีชื่อผู้ใช้งานในระบบแล้ว',
            'email.unique'    => 'มีอีเมล์นี้ในระบบแล้ว',
            'phone.unique'    => 'มีเบอร์โทรนี้ในระบบแล้ว',
            'image.mimes'     => 'กรุณาเลือกไฟล์รูปภาพ'
        ];
    }
}
