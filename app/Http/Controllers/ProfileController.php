<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Profile\Update;
use Auth;
use Hash;

class ProfileController extends Controller
{
    public function index()
    {
        return view('profile.index');
    }

    public function edit()
    {
        return view('profile.edit');
    }

    public function update(Update $request)
    {
        $user = Auth::user();

        if(isset($request->password)):
            if (Hash::check($request->old_password, $user->password)):
                $user->password = $request->password;
            else :
                return redirect()->back()->withErrors(['old_password'=> 'พาสเวิร์ดเก่าไม่ถูกต้อง']);
            endif;
        endif;

        $user->name     = $request->name;
        $user->username = $request->username;
        $user->email    = $request->email;
        $user->phone    = $request->phone;
        $user->image    = $request->hasFile('image') ? uploadImageUser(['image' => $request->image , 'edit_image' => $request->edit_image]) : $request->edit_image;
        $user->save();

        return redirect()->route('profile.index')->with('update', 'Success');
    }
}
