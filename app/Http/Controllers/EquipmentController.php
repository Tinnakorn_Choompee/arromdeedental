<?php

namespace App\Http\Controllers;

use App\Models\Equipment;
use App\Models\Unit;
use Illuminate\Http\Request;

class EquipmentController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:equipment-list');
        $this->middleware('permission:equipment-create', ['only' => ['create','store']]);
        $this->middleware('permission:equipment-edit'  , ['only' => ['edit','update']]);
        $this->middleware('permission:equipment-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $equipment = Equipment::get();
        return view('equipment.index')->with(['equipment' => $equipment]);
    }

    public function create()
    {
        return view('equipment.create')->with(['type' => Unit::where('type', 'equipment')->pluck('name', 'name')]);
    }

    public function store(Request $request)
    {
        $rules    = ['code' => 'required|unique:equipment', 'name' => 'required|unique:equipment'];
        $messages = ['code.unique' => 'มีรหัสอุปกรณ์นี้อยุ่ในระบบแล้ว', 'name.unique' => 'มีชื่ออุปกรณ์นี้อยู่ในระบบแล้ว'];
        $request->validate($rules,$messages);
        Equipment::create($request->all());
        return redirect()->route('equipment.index')->with('success','Successfully');
    }

    public function show(Equipment $equipment)
    {
        return view('equipment.show')->with(['equipment' => $equipment]);
    }

    public function edit(Equipment $equipment)
    {
        return view('equipment.edit')->with(['equipment' => $equipment])->with(['type' => Unit::where('type', 'equipment')->pluck('name', 'name')]);
    }

    public function update(Request $request, Equipment $equipment)
    {
        $rules    = ['code' => 'required|unique:equipment,code,'.$equipment->id, 'name' => 'required|unique:equipment,name,'.$equipment->id];
        $messages = ['code.unique' => 'มีรหัสอุปกรณ์นี้อยุ่ในระบบแล้ว', 'name.unique' => 'มีชื่ออุปกรณ์นี้อยู่ในระบบแล้ว'];
        $request->validate($rules,$messages);
        Equipment::updateOrCreate(['id'=>$equipment->id],$request->all());
        return redirect()->route('equipment.index')->with('update','Successfully');
    }

    public function destroy(Equipment $equipment)
    {
        Equipment::destroy($equipment->id);
        return redirect()->route('equipment.index')->with('delete','Successfully');
    }
}
