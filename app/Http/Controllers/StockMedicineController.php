<?php

namespace App\Http\Controllers;

use App\Models\Stock;
use Illuminate\Http\Request;
use Auth;

class StockMedicineController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:stock_medicines-list');
        $this->middleware('permission:stock_medicines-create', ['only' => ['create','store']]);
        $this->middleware('permission:stock_medicines-delete', ['only' => ['delete']]);
    }

    public function index()
    {
        $medicines = Stock::where('type', 'medicine')->GetDesc();
        return view('stock.medicines.index')->with(['medicines' => $medicines]);
    }

    public function store(Request $request)
    {
        $stock = new Stock;
        $stock->type         = "medicine";
        $stock->user_id      = Auth::user()->id;
        $stock->medicine_id  = $request->id;
        $stock->qty          = $request->qty;
        $stock->save();
        $stock->medicine->amount = $stock->medicine->amount + $request->qty;
        $stock->medicine->save();
        return response()->json("success");
    }

    public function delete(Request $request)
    {
        Stock::destroy($request->id);
        return redirect()->back()->with('delete', 'Delete Successfully !');
    }
}
