<?php

namespace App\Http\Controllers;

use App\Models\Reimbursement;
use App\Models\Medicine;
use Illuminate\Http\Request;
use Carbon;

class ReimbursementMedicinesController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:reimbursement_medicines-list');
        $this->middleware('permission:reimbursement_medicines-create', ['only' => ['create','store']]);
        $this->middleware('permission:reimbursement_medicines-edit'  , ['only' => ['edit','update']]);
        $this->middleware('permission:reimbursement_medicines-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $medicines = Reimbursement::where('type','medicine')->get();
        return view('reimbursement.medicines.index')->with(['medicines' => $medicines]);
    }

    public function create()
    {
        $medicines = Medicine::where('amount', '>', 0)->pluck('name', 'id');
        return view('reimbursement.medicines.create')->with(['medicines' => $medicines]);
    }

    public function store(Request $request)
    {
        $medicines = Medicine::find($request->medicine_id);

        $rules    = [
            'medicine_id' => 'required', 
            'code'        => 'required',
            'disburse'    => 'required|numeric|min:1|max:'.$medicines->amount
        ];

        $messages = [
            'disburse.min' => 'กรุณาเบิกยาตั้งแต่ 1 ขึ้นไป', 
            'disburse.max' => 'เบิกยามากกว่าจำนวนที่มี'
        ];

        $request->validate($rules,$messages);
        $medicines->amount = $medicines->amount - $request->disburse;
        $medicines->save();

        $reimbursement = new Reimbursement;
        $reimbursement->type        = "medicine";
        $reimbursement->status      = "disburse";
        $reimbursement->medicine_id = $request->medicine_id;
        $reimbursement->disburse    = $request->disburse;
        $reimbursement->balance     = $medicines->amount;
        $reimbursement->save();

        return redirect()->route('reimbursement_medicines.index')->with('success','Successfully');
    }

    public function show(Reimbursement $reimbursement_medicine)
    {
        return view('reimbursement.medicines.show')->with(['reimbursement_medicine' => $reimbursement_medicine]);
    }

    public function edit(Reimbursement $reimbursement_medicine)
    {
        $medicines = Medicine::where('amount', '>', 0)->pluck('name', 'id');
        return view('reimbursement.medicines.edit')->with(['medicines' => $medicines, 'reimbursement_medicine' => $reimbursement_medicine]);
    }

    public function update(Request $request, Reimbursement $reimbursement_medicine)
    {
        $medicines  = Medicine::find($request->medicine_id);
        $amount     = $medicines->amount + $reimbursement_medicine->disburse;
        
        $rules  = [
            'medicine_id' => 'required', 
            'code'        => 'required',
            'disburse'    => 'required|numeric|min:1|max:'.$amount
        ];

        $messages = [
            'disburse.min' => 'กรุณาเบิกยาตั้งแต่ 1 ขึ้นไป', 
            'disburse.max' => 'เบิกยามากกว่าจำนวนที่มี'
        ];

        $request->validate($rules,$messages);

        $medicines->amount = $amount - $request->disburse;
        $medicines->save();

        $reimbursement_medicine->medicine_id = $request->medicine_id;
        $reimbursement_medicine->disburse    = $request->disburse;
        $reimbursement_medicine->balance     = $medicines->amount;
        $reimbursement_medicine->save();

        return redirect()->route('reimbursement_medicines.index')->with('update','Successfully');
    }

    public function destroy(Reimbursement $reimbursement_medicine)
    {
        switch($reimbursement_medicine->status):
            case "disburse" : 
                $medicines = Medicine::find($reimbursement_medicine->medicine_id);
                $medicines->amount = $medicines->amount + $reimbursement_medicine->disburse;
                $medicines->save();
            break;
        endswitch;
        Reimbursement::destroy($reimbursement_medicine->id);
        return redirect()->route('reimbursement_medicines.index')->with('delete','Successfully');
    }

    public function takeback(Request $request)
    {
        $reimbursement = Reimbursement::find($request->id);
        $medicines     = Medicine::find($reimbursement->medicine_id);
        $rules    = ['takeback' => 'required|numeric|min:0|max:'.$reimbursement->disburse];
        $messages = ['takeback.min' => 'กรุณาคืนยาตั้งแต่ 0 ขึ้นไป','takeback.max' => 'จำนวนส่งคืนต้องน้อยกว่าหรือเท่ากับ '.$reimbursement->disburse];
        $request->validate($rules,$messages);

        $reimbursement->takeback = $request->takeback;
        $reimbursement->status   = "takeback";

        switch($request->takeback):
            case ($reimbursement->disburse) :
                $reimbursement->balance = $medicines->amount + $request->takeback;
            break;
            case (0) :
                $reimbursement->balance = $medicines->amount;
            break;
            default :
                $reimbursement->balance = $medicines->amount + ($reimbursement->disburse - $request->takeback);
        endswitch;
        
        $reimbursement->datetime = Carbon::now();
        $reimbursement->save();

        $medicines->amount = $reimbursement->balance;
        $medicines->save();

        return redirect()->route('reimbursement_medicines.index')->with('success','Successfully');
    }
}
