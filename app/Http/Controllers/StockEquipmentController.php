<?php

namespace App\Http\Controllers;

use App\Models\Stock;
use Illuminate\Http\Request;
use Auth;

class StockEquipmentController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:stock_equipment-list');
        $this->middleware('permission:stock_equipment-create', ['only' => ['create','store']]);
        $this->middleware('permission:stock_equipment-delete', ['only' => ['delete']]);
    }

    public function index()
    {
        $equipment = Stock::where('type', 'equipment')->GetDesc();
        return view('stock.equipment.index')->with(['equipment' => $equipment]);
    }

    public function store(Request $request)
    {
        $stock = new Stock;
        $stock->type         = "equipment";
        $stock->user_id      = Auth::user()->id;
        $stock->equipment_id = $request->id;
        $stock->qty          = $request->qty;
        $stock->save();
        $stock->equipment->amount = $stock->equipment->amount + $request->qty;
        $stock->equipment->save();
        return response()->json("success");
    }

    public function delete(Request $request)
    {
        Stock::destroy($request->id);
        return redirect()->back()->with('delete', 'Delete Successfully !');
    }
}
