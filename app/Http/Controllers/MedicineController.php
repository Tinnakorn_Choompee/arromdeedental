<?php

namespace App\Http\Controllers;

use App\Models\Medicine;
use App\Models\Unit;
use Illuminate\Http\Request;

class MedicineController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:medicines-list');
        $this->middleware('permission:medicines-create', ['only' => ['create','store']]);
        $this->middleware('permission:medicines-edit'  , ['only' => ['edit','update']]);
        $this->middleware('permission:medicines-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $medicines = Medicine::get();
        return view('medicines.index')->with(['medicines' => $medicines]);
    }

    public function create()
    {
        return view('medicines.create')->with(['type' => Unit::where('type', 'medicine')->pluck('name', 'name')]);
    }

    public function store(Request $request)
    {
        $rules    = ['code' => 'required|unique:medicines', 'name' => 'required|unique:medicines'];
        $messages = ['code.unique' => 'มีรหัสยานี้อยุ่ในระบบแล้ว', 'name.unique' => 'มีชื่อยานี้อยู่ในระบบแล้ว'];
        $request->validate($rules,$messages);
        Medicine::create($request->all());
        return redirect()->route('medicines.index')->with('success','Successfully');

    }

    public function show(Medicine $medicine)
    {
        return view('medicines.show')->with(['medicine' => $medicine]);
    }

    public function edit(Medicine $medicine)
    {
        return view('medicines.edit')->with(['medicine' => $medicine])->with(['type' => Unit::where('type', 'medicine')->pluck('name', 'name')]);
    }

    public function update(Request $request, Medicine $medicine)
    {
        $rules    = ['code' => 'required|unique:medicines,code,'.$medicine->id, 'name' => 'required|unique:medicines,name,'.$medicine->id];
        $messages = ['code.unique' => 'มีรหัสยานี้อยุ่ในระบบแล้ว', 'name.unique' => 'มีชื่อยานี้อยู่ในระบบแล้ว'];
        $request->validate($rules,$messages);
        Medicine::updateOrCreate(['id'=>$medicine->id],$request->all());
        return redirect()->route('medicines.index')->with('update','Successfully');
    }

    public function destroy(Medicine $medicine)
    {
        Medicine::destroy($medicine->id);
        return redirect()->route('medicines.index')->with('delete','Successfully');
    }
}
