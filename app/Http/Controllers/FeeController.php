<?php

namespace App\Http\Controllers;

use App\Models\Fee;
use Illuminate\Http\Request;

class FeeController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Fee $fee)
    {
        //
    }

    public function edit(Fee $fee)
    {
        //
    }

    public function update(Request $request, Fee $fee)
    {
        //
    }

    public function destroy(Fee $fee)
    {
        //
    }
}
