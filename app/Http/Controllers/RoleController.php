<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;

class RoleController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:role-list');
        $this->middleware('permission:role-edit'  , ['only' => ['edit','update']]);
    }

    public function index()
    {
        $roles = Role::all();
        return view('role.index',compact('roles'));
    }

    public function show(Role $role)
    {
        $rolePermissions = Permission::join("role_has_permissions", "role_has_permissions.permission_id","permissions.id")->where("role_has_permissions.role_id",$role->id)->get();
        return view('role.show',compact('role','rolePermissions'));
    }

    public function edit(Role $role)
    {
        $permission      = Permission::get();
        $rolePermissions = DB::table("role_has_permissions")
        ->where("role_has_permissions.role_id",$role->id)
        ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
        ->all();
        return view('role.edit', compact('role','permission','action','rolePermissions'));
    }

    public function update(Request $request, Role $role)
    {
        $message = ['name.unique'  => 'มีชื่อสิทธิ์การใช้งานนี้ในระบบแล้ว'];
        $request->validate(['name' => 'required|unique:roles,name,'.$role->id], $message);
        $role->syncPermissions($request->permission);
        return redirect()->route('role.index')->with('update','Updated Successfully');
    }
}
