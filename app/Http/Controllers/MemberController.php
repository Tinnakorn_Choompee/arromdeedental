<?php

namespace App\Http\Controllers;

use App\Models\Member;
use App\Http\Requests\Member\Create;
use App\Http\Requests\Member\Update;

class MemberController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:member-list');
        $this->middleware('permission:member-create', ['only' => ['create','store']]);
        $this->middleware('permission:member-edit'  , ['only' => ['edit','update']]);
        $this->middleware('permission:member-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $member = Member::get();
        return view('member.index')->withMember($member);
    }

    public function create()
    {
        return view('member.create');
    }

    public function store(Create $request)
    {
        $member = Member::create($request->except(['image', 'birthday']));
        $member->birthday = $request->birthday ? birthday($request->birthday) : NULL;
        $member->age      = $request->birthday ? $request->age : NULL;
        $member->image    = $request->hasFile('image') ? uploadImageUser(['image' => $request->image, 'edit_image' => NULL]) : "member.png";
        $member->save();
        return redirect()->route('member.index')->with('success','Successfully');
    }

    public function show(Member $member)
    {
        return view('member.show')->withMember($member);
    }

    public function edit(Member $member)
    {
        return view('member.edit')->withMember($member);
    }

    public function update(Update $request, Member $member)
    {
        $member = Member::updateOrCreate(['id'=>$member->id],$request->except(['birthday','image']));
        $member->birthday = $request->birthday ? birthday($request->birthday) : NULL;
        $member->age      = $request->birthday ? $request->age : NULL;
        $member->image    = $request->hasFile('image') ? uploadImageUser(['image' => $request->image, 'edit_image' => $request->edit_image]) : $request->edit_image;
        $member->save();
        return redirect()->route('member.index')->with('update','Successfully');
    }

    public function destroy(Member $member)
    {
        $member->image == 'member.png' ? : File::delete('image/users/'.$member->image);
        Member::destroy($member->id);
        return redirect()->route('member.index')->with('delete','Successfully');
    }
}
