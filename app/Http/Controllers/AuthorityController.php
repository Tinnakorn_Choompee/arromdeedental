<?php

namespace App\Http\Controllers;

use App\Http\Requests\Authority\Create;
use App\Http\Requests\Authority\Update;
use App\Models\User;
use File;

class AuthorityController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:authority-list');
        $this->middleware('permission:authority-create', ['only' => ['create','store']]);
        $this->middleware('permission:authority-edit'  , ['only' => ['edit','update']]);
        $this->middleware('permission:authority-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $authority = User::role('authority')->get();
        return view('authority.index')->withAuthority($authority);
    }

    public function create()
    {
        return view('authority.create');
    }

    public function store(Create $request)
    {
        $user = User::create($request->except(['image']));
        $user->image = $request->hasFile('image') ? uploadImageUser(['image' => $request->image, 'edit_image' => NULL]) : "user.png";
        $user->save();
        $user->assignRole("authority");
        return redirect()->route('authority.index')->with('success','Successfully');
    }

    public function show(User $authority)
    {
        return view('authority.show')->withAuthority($authority);
    }

    public function edit(User $authority)
    {
        return view('authority.edit')->withAuthority($authority);
    }

    public function update(Update $request, User $authority)
    {
        $user = User::updateOrCreate(['id'=>$authority->id],$request->except(['password','image']));
        isset($request->password) ? $user->password = $request->password : NULL;
        $user->image = $request->hasFile('image') ? uploadImageUser(['image' => $request->image,'edit_image' => $request->edit_image]) : $request->edit_image;
        $user->save();
        return redirect()->route('authority.index')->with('update','Successfully');
    }

    public function destroy(User $authority)
    {
        $authority->image == 'user.png' ? : File::delete('image/users/'.$authority->image);
        User::destroy($authority->id);
        return redirect()->route('authority.index')->with('delete','Successfully');
    }
}
