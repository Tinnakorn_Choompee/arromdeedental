<?php

namespace App\Http\Controllers;

use App\Models\Reimbursement;
use App\Models\Equipment;
use Illuminate\Http\Request;
use Carbon;

class ReimbursementEquipmentController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:reimbursement_equipment-list');
        $this->middleware('permission:reimbursement_equipment-create', ['only' => ['create','store']]);
        $this->middleware('permission:reimbursement_equipment-edit'  , ['only' => ['edit','update']]);
        $this->middleware('permission:reimbursement_equipment-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $equipment = Reimbursement::where('type','equipment')->get();
        return view('reimbursement.equipment.index')->with(['equipment' => $equipment]);
    }

    public function create()
    {
        $equipment = Equipment::where('amount', '>', 0)->pluck('name', 'id');
        return view('reimbursement.equipment.create')->with(['equipment' => $equipment]);
    }

    public function store(Request $request)
    {
        $equipment = Equipment::find($request->equipment_id);

        $rules    = [
            'equipment_id' => 'required', 
            'code'        => 'required',
            'disburse'    => 'required|numeric|min:1|max:'.$equipment->amount
        ];

        $messages = [
            'disburse.min' => 'กรุณาเบิกอุปกรณ์ตั้งแต่ 1 ขึ้นไป', 
            'disburse.max' => 'เบิกอุปกรณ์มากกว่าจำนวนที่มี'
        ];

        $request->validate($rules,$messages);
        $equipment->amount = $equipment->amount - $request->disburse;
        $equipment->save();

        $reimbursement = new Reimbursement;
        $reimbursement->type        = "equipment";
        $reimbursement->status      = "disburse";
        $reimbursement->equipment_id = $request->equipment_id;
        $reimbursement->disburse    = $request->disburse;
        $reimbursement->balance     = $equipment->amount;
        $reimbursement->save();

        return redirect()->route('reimbursement_equipment.index')->with('success','Successfully');
    }

    public function show(Reimbursement $reimbursement_equipment)
    {
        return view('reimbursement.equipment.show')->with(['reimbursement_equipment' => $reimbursement_equipment]);
    }

    public function edit(Reimbursement $reimbursement_equipment)
    {
        $equipment = Equipment::where('amount', '>', 0)->pluck('name', 'id');
        return view('reimbursement.equipment.edit')->with(['equipment' => $equipment, 'reimbursement_equipment' => $reimbursement_equipment]);
    }

    public function update(Request $request, Reimbursement $reimbursement_equipment)
    {
        $equipment  = Equipment::find($request->equipment_id);
        $amount     = $equipment->amount + $reimbursement_equipment->disburse;
        
        $rules  = [
            'equipment_id' => 'required', 
            'code'        => 'required',
            'disburse'    => 'required|numeric|min:1|max:'.$amount
        ];

        $messages = [
            'disburse.min' => 'กรุณาเบิกอุปกรณ์ตั้งแต่ 1 ขึ้นไป', 
            'disburse.max' => 'เบิกอุปกรณ์มากกว่าจำนวนที่มี'
        ];

        $request->validate($rules,$messages);

        $equipment->amount = $amount - $request->disburse;
        $equipment->save();

        $reimbursement_equipment->equipment_id = $request->equipment_id;
        $reimbursement_equipment->disburse     = $request->disburse;
        $reimbursement_equipment->balance      = $equipment->amount;
        $reimbursement_equipment->save();

        return redirect()->route('reimbursement_equipment.index')->with('update','Successfully');
    }

    public function destroy(Reimbursement $reimbursement_equipment)
    {
        switch($reimbursement_equipment->status):
            case "disburse" : 
                $equipment = Equipment::find($reimbursement_equipment->equipment_id);
                $equipment->amount = $equipment->amount + $reimbursement_equipment->disburse;
                $equipment->save();
            break;
        endswitch;
        Reimbursement::destroy($reimbursement_equipment->id);
        return redirect()->route('reimbursement_equipment.index')->with('delete','Successfully');
    }

    public function takeback(Request $request)
    {
        $reimbursement = Reimbursement::find($request->id);
        $equipment     = Equipment::find($reimbursement->equipment_id);
        $rules    = ['takeback' => 'required|numeric|min:0|max:'.$reimbursement->disburse];
        $messages = ['takeback.min' => 'กรุณาคืนอุปกรณ์ตั้งแต่ 0 ขึ้นไป','takeback.max' => 'จำนวนส่งคืนต้องน้อยกว่าหรือเท่ากับ '.$reimbursement->disburse];
        $request->validate($rules,$messages);

        $reimbursement->takeback = $request->takeback;
        $reimbursement->status   = "takeback";

        switch($request->takeback):
            case ($reimbursement->disburse) :
                $reimbursement->balance = $equipment->amount + $request->takeback;
            break;
            case (0) :
                $reimbursement->balance = $equipment->amount;
            break;
            default :
                $reimbursement->balance = $equipment->amount + ($reimbursement->disburse - $request->takeback);
        endswitch;
        
        $reimbursement->datetime = Carbon::now();
        $reimbursement->save();

        $equipment->amount = $reimbursement->balance;
        $equipment->save();

        return redirect()->route('reimbursement_equipment.index')->with('success','Successfully');
    }
}
