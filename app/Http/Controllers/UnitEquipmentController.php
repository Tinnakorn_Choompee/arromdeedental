<?php

namespace App\Http\Controllers;

use App\Models\Unit;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UnitEquipmentController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:type_equipment-list');
        $this->middleware('permission:type_equipment-create', ['only' => ['create','store']]);
        $this->middleware('permission:type_equipment-edit'  , ['only' => ['edit','update']]);
        $this->middleware('permission:type_equipment-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $equipment = Unit::where('type','equipment')->get();
        return view('units.equipment.index')->with(['equipment' => $equipment]);
    }

    public function create()
    {
        return view('units.equipment.create');
    }

    public function store(Request $request)
    {
        $request->validate(['name' => Rule::unique('units')->where('type', $request->type)],['name.unique' => 'มีชื่อประเภทอุปกรณ์นี้ในระบบแล้ว']);
        Unit::create($request->all());
        return redirect()->route('type_equipment.index')->with('success','Successfully');
    }

    public function show(Unit $type_equipment)
    {
        return view('units.equipment.show')->with(['equipment' => $type_equipment]);
    }

    public function edit(Unit $type_equipment)
    {
        return view('units.equipment.edit')->with(['equipment' => $type_equipment]);
    }

    public function update(Request $request, Unit $type_equipment)
    {
        $request->validate(['name' => Rule::unique('units')->where('type', $type_equipment->type)->ignore($type_equipment->id)], ['name.unique' => 'มีชื่อประเภทอุปกรณ์นี้ในระบบแล้ว']);
        Unit::updateOrCreate(['id'=>$type_equipment->id],$request->all());
        return redirect()->route('type_equipment.index')->with('update','Successfully');
    }

    public function destroy(Unit $type_equipment)
    {
        Unit::destroy($type_equipment->id);
        return redirect()->route('type_equipment.index')->with('delete','Successfully');
    }
}
