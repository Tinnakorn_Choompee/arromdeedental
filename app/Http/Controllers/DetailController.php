<?php

namespace App\Http\Controllers;

use App\Models\Detail;
use Illuminate\Http\Request;

class DetailController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Detail $detail)
    {
        //
    }

    public function edit(Detail $detail)
    {
        //
    }

    public function update(Request $request, Detail $detail)
    {
        //
    }
    
    public function destroy(Detail $detail)
    {
        //
    }
}
