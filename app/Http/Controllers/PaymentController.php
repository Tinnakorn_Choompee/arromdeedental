<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Auth;
use PDF;

class PaymentController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:payments-list');
    }

    public function index()
    {
        $payments = Payment::GetDesc();
        return view("payment.index")->with(['payments' => $payments]);
    }

    public function show(Payment $payment)
    {
        return view('payment.show')->with(['payment' => $payment]);
    }

    public function update(Payment $payment)
    {
        $payment->status  = "success";
        $payment->user_id = Auth::user()->id;
        $payment->save();
        return redirect()->route("payments.index")->with('success','Successfully');
    }

    public function print(Payment $payment)
    {
        return PDF::loadView('payment.print', compact('payment'))->download('payment.pdf');
    }
}
