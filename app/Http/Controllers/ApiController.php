<?php

namespace App\Http\Controllers;

use App\Models\Medicine;
use App\Models\Equipment;
use App\Models\FeeList;

class ApiController extends Controller
{
    public function medicines(Medicine $medicine)
    {
        return response()->json($medicine);
    }

    public function equipment(Equipment $equipment)
    {
        return response()->json($equipment);
    }

    public function fees($fees)
    {
        return response()->json(FeeList::find($fees));
    }
}
