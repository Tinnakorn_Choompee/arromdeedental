<?php

namespace App\Http\Controllers;

use App\Http\Requests\Dentist\Create;
use App\Http\Requests\Dentist\Update;
use App\Models\User;
use File;

class DentistController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:dentist-list');
        $this->middleware('permission:dentist-create', ['only' => ['create','store']]);
        $this->middleware('permission:dentist-edit'  , ['only' => ['edit','update']]);
        $this->middleware('permission:dentist-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $dentist = User::role('dentist')->get();
        return view('dentist.index')->withDentist($dentist);
    }

    public function create()
    {
        return view('dentist.create');
    }

    public function store(Create $request)
    {
        $user = User::create($request->except(['image']));
        $user->image = $request->hasFile('image') ? uploadImageUser(['image' => $request->image, 'edit_image' => NULL]) : "doctor.png";
        $user->save();
        $user->assignRole("dentist");
        return redirect()->route('dentist.index')->with('success','Successfully');
    }

    public function show(User $dentist)
    {
        return view('dentist.show')->withDentist($dentist);
    }

    public function edit(User $dentist)
    {
        return view('dentist.edit')->withDentist($dentist);
    }

    public function update(Update $request, User $dentist)
    {
        $user = User::updateOrCreate(['id'=>$dentist->id],$request->except(['password','image']));
        isset($request->password) ? $user->password = $request->password : NULL;
        $user->image = $request->hasFile('image') ? uploadImageUser(['image' => $request->image,'edit_image' => $request->edit_image]) : $request->edit_image;
        $user->save();
        return redirect()->route('dentist.index')->with('update','Successfully');
    }

    public function destroy(User $dentist)
    {
        $dentist->image == 'doctor.png' ? : File::delete('image/users/'.$dentist->image);
        User::destroy($dentist->id);
        return redirect()->route('dentist.index')->with('delete','Successfully');
    }
}
