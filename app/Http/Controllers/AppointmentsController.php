<?php

namespace App\Http\Controllers;

use App\Models\Appointments;
use App\Models\User;
use App\Models\Member;
use Illuminate\Http\Request;
use Carbon;
use Auth;

class AppointmentsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:appointments-list');
        $this->middleware('permission:appointments-create', ['only' => ['create','store']]);
        $this->middleware('permission:appointments-edit'  , ['only' => ['edit','update']]);
        $this->middleware('permission:appointments-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        switch(Auth::user()->getRoleNames()->first()):
            case "admin" :
            case "authority" :
                if ($request->has('date')):
                    $date   = explode(" ถึง ",$request->date);
                    $from  = Carbon::parse($date[0]);
                    $to    = Carbon::parse($date[1])->addDays(1);
                    $appointments = Appointments::whereBetween('datetime', [$from, $to])->get();
                else:
                    $appointments = Appointments::all();
                endif;
            break;
            case "dentist" :
                $appointments = Appointments::where('dentist_id', Auth::user()->id)->whereDate('datetime', Carbon::now()->format('Y-m-d'))->get();
            break;
        endswitch;
        return view('appointments.index')->with(['appointments' => $appointments, 'date_range' => $request->date]);
    }

    public function create()
    {
        $dentist = User::role('dentist')->GetDesc()->pluck('name', 'id');
        $member  = Member::GetDesc()->pluck('name', 'id');
        return view('appointments.create')->with(['dentist' => $dentist])->with(['member'=> $member]);
    }

    public function store(Request $request)
    {
        $date = explode("-",$request->date);
        $time = explode(":",$request->time);
        $datetime = Carbon::create($date[0], $date[1], $date[2], $time[0], $time[1]);
        $appointments = new Appointments;
        $appointments->dentist_id = $request->dentist_id;
        $appointments->member_id  = $request->member_id;
        $appointments->user_id    = Auth::user()->id;
        $appointments->datetime   = $datetime;
        $appointments->detail     = $request->detail;
        $appointments->status     = "proceed";
        $appointments->save();
        return redirect()->route('appointments.index')->with('success','Successfully');
    }

    public function show(Appointments $appointment)
    {
       return view('appointments.show')->with(['appointment' => $appointment]);
    }

    public function edit(Appointments $appointment)
    {
       $dentist = User::role('dentist')->GetDesc()->pluck('name', 'id');
       $member  = Member::GetDesc()->pluck('name', 'id');
       return view('appointments.edit')->with(['appointment' => $appointment])->with(['dentist' => $dentist])->with(['member'=> $member]);
    }

    public function update(Request $request, Appointments $appointment)
    {
        $date = explode("-",$request->date);
        $time = explode(":",$request->time);
        $datetime = Carbon::create($date[0], $date[1], $date[2], $time[0], $time[1]);
        $appointment->dentist_id = $request->dentist_id;
        $appointment->member_id  = $request->member_id;
        $appointment->datetime   = $datetime;
        $appointment->detail     = $request->detail;
        $appointment->save();
        return redirect()->route('appointments.index')->with('success','Successfully');
    }

    public function destroy(Appointments $appointment)
    {
        Appointments::destroy($appointment->id);
        return redirect()->route('appointments.index')->with('delete','Successfully');
    }

    public function success(Request $request)
    {
        $appointment = Appointments::find($request->id);
        $appointment->status = "success";
        $appointment->save();
        return response()->json('success');
    }
}
