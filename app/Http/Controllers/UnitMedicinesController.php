<?php

namespace App\Http\Controllers;

use App\Models\Unit;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UnitMedicinesController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:type_medicines-list');
        $this->middleware('permission:type_medicines-create', ['only' => ['create','store']]);
        $this->middleware('permission:type_medicines-edit'  , ['only' => ['edit','update']]);
        $this->middleware('permission:type_medicines-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $medicines = Unit::where('type','medicine')->get();
        return view('units.midicines.index')->with(['medicines' => $medicines]);
    }

    public function create()
    {
        return view('units.midicines.create');
    }

    public function store(Request $request)
    {
        $request->validate(['name' => Rule::unique('units')->where('type', $request->type)],['name.unique' => 'มีชื่อประเภทยานี้ในระบบแล้ว']);
        Unit::create($request->all());
        return redirect()->route('type_medicines.index')->with('success','Successfully');
    }

    public function show(Unit $type_medicine)
    {
        return view('units.midicines.show')->with(['medicines' => $type_medicine]);
    }

    public function edit(Unit $type_medicine)
    {
        return view('units.midicines.edit')->with(['medicines' => $type_medicine]);
    }

    public function update(Request $request, Unit $type_medicine)
    {
        $request->validate(['name' => Rule::unique('units')->where('type', $type_medicine->type)->ignore($type_medicine->id)], ['name.unique' => 'มีชื่อประเภทยานี้ในระบบแล้ว']);
        Unit::updateOrCreate(['id'=>$type_medicine->id],$request->all());
        return redirect()->route('type_medicines.index')->with('update','Successfully');
    }

    public function destroy(Unit $type_medicine)
    {
        Unit::destroy($type_medicine->id);
        return redirect()->route('type_medicines.index')->with('delete','Successfully');
    }
}
