<?php

namespace App\Http\Controllers;

use App\Models\Treatment;
use App\Models\Appointments;
use App\Models\Detail;
use App\Models\Member;
use App\Models\Fee;
use App\Models\FeeList;
use App\Models\Payment;
use Illuminate\Http\Request;
use Auth;
use File;

class TreatmentController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:treatments-list');
        $this->middleware('permission:treatments-create', ['only' => ['create','store']]);
        $this->middleware('permission:treatments-edit'  , ['only' => ['edit','update']]);
        $this->middleware('permission:treatments-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        switch(Auth::user()->getRoleNames()->first()):
            case "admin" :
            case "authority" :
                $treatments = Treatment::GetDesc();
            break;
            case "dentist" :
                $treatments = Treatment::where('dentist_id', Auth::user()->id)->GetDesc();
            break;
        endswitch;
        return view('treatment.index')->with(['treatments' => $treatments]);
    }

    public function create(Request $request)
    {
        if ($request->has('appointment')):
            $appointments = Appointments::where('id', $request->appointment)->where('dentist_id', Auth::user()->id)->first();
        else:
            $appointments = null;
        endif;

        return view('treatment.create')
                ->with(['fees'         => Fee::whereBetween('id', [1,9])->get()])
                ->with(['medicines'    => Fee::whereBetween('id', [10,13])->get()])
                ->with(['appointments' => $appointments])
                ->with(['member'       => Member::GetDesc()->pluck('name', 'id')]);
    }

    public function store(Request $request)
    {
        $rules    = ['fees'          => 'required'];
        $messages = ['fees.required' => 'กรุณาเลือกการรักษา'];
        $request->validate($rules, $messages);

        $member = Member::find($request->member_id);

        $film = $request->hasFile('image') ? uploadImageFilm(['image' => $request->image, 'edit_image' => NULL]) : NULL;

        foreach($request->fees as $rs):
            $value  = (explode("-",$rs));
            $result = FeeList::find($value[0]);
            $fee[] = ['id'=> $result->id ,'name' => $result->name, 'price' => $result->price];
        endforeach;

        if($request->medicines):
            foreach($request->medicines as $rs):
                $value  = (explode("-",$rs));
                $result = FeeList::find($value[0]);
                $medicine[] = ['id'=> $result->id ,'name' => $result->name, 'price' => $result->price];
            endforeach;
        else:
            $medicine = null;
        endif;

        session(['title'     => $request->title]);
        session(['member_id' => $member->id]);
        session(['name'      => $member->name]);
        session(['fees'      => $fee]);
        session(['medicines' => $medicine]);
        session(['film'      => $film]);

        return redirect()->route('treatments.confirm');
    }

    public function show(Treatment $treatment)
    {
        return view('treatment.show')->with(['treatment' => $treatment]);
    }

    public function edit(Treatment $treatment)
    {
        foreach($treatment->details_treatment as $rs):
            $details_treatment[] = $rs->fee_list_id;
        endforeach;

        foreach($treatment->details_medicine as $rs):
            $details_medicine[] = $rs->fee_list_id;
        endforeach;

        return view('treatment.edit')
               ->with(['details_treatment' => $details_treatment])
               ->with(['details_medicine'  => isset($details_medicine) ? $details_medicine : NULL])
               ->with(['treatment' => $treatment])
               ->with(['fees'      => Fee::whereBetween('id', [1,9])->get()])
               ->with(['medicines' => Fee::whereBetween('id', [10,13])->get()])
               ->with(['member'    => Member::GetDesc()->pluck('name', 'id')]);
    }

    public function update(Request $request, Treatment $treatment)
    {
        $rules    = ['fees'          => 'required'];
        $messages = ['fees.required' => 'กรุณาเลือกการรักษา'];
        $request->validate($rules, $messages);

        $member = Member::find($request->member_id);

        $film = $request->hasFile('image') ? uploadImageFilm(['image' => $request->image, 'edit_image' => $request->edit_image]) : $request->edit_image;

        foreach($request->fees as $rs):
            $value  = (explode("-",$rs));
            $result = FeeList::find($value[0]);
            $fee[] = ['id'=> $result->id ,'name' => $result->name, 'price' => $result->price];
        endforeach;

        if($request->medicines):
            foreach($request->medicines as $rs):
                $value  = (explode("-",$rs));
                $result = FeeList::find($value[0]);
                $medicine[] = ['id'=> $result->id ,'name' => $result->name, 'price' => $result->price];
            endforeach;
        else:
            $medicine = null;
        endif;

        session(['id'        => $treatment->id]);
        session(['title'     => $request->title]);
        session(['member_id' => $member->id]);
        session(['name'      => $member->name]);
        session(['fees'      => $fee]);
        session(['medicines' => $medicine]);
        session(['film'      => $film]);

        return redirect()->route('treatments.confirm_change');
    }

    public function destroy(Treatment $treatment)
    {
        $treatment->image == NULL ? : File::delete('image/film/'.$treatment->image);
        Treatment::destroy($treatment->id);
        return redirect()->route('treatments.index')->with('delete','Successfully');
    }

    public function confirm()
    {
        return view('treatment.confirm');
    }

    public function confirm_change()
    {
        return view('treatment.confirm_change');
    }

    public function save(Request $request)
    {
        session()->forget(['title', 'member_id', 'name', 'fees', 'medicines', 'film']);

        $medicine_price = $request->medicine ? array_sum($request->medicine_price) : 0;
        $total = array_sum($request->price) + $medicine_price;

        $treatment = new Treatment;
        $treatment->dentist_id = Auth::user()->id;
        $treatment->member_id  = $request->member_id;
        $treatment->title      = $request->title;
        $treatment->total      = $total;
        $treatment->image      = $request->film;
        $treatment->save();

        foreach($request->id as $k => $rs):
            $detail = new Detail;
            $detail->treatment_id = $treatment->id;
            $detail->fee_list_id  = $rs;
            $detail->name         = $request->name[$k];
            $detail->price        = $request->price[$k];
            $detail->type         = "treatment";
            $detail->save();
        endforeach;

        if($request->medicine):
            foreach($request->medicine_id as $k => $rs):
                $detail = new Detail;
                $detail->treatment_id = $treatment->id;
                $detail->fee_list_id  = $rs;
                $detail->name         = $request->medicine_name[$k];
                $detail->price        = $request->medicine_price[$k];
                $detail->type       = "medicine";
                $detail->save();
            endforeach;
        endif;

        $payment = new Payment;
        $payment->treatment_id = $treatment->id;
        $payment->status       = "proceed";
        $payment->save();

        return redirect()->route('treatments.index')->with('success','Successfully');
    }

    public function change(Request $request, Treatment $treatment)
    {
        session()->forget(['id', 'title', 'member_id', 'name', 'fees', 'medicines', 'film']);

        $medicine_price = $request->medicine ? array_sum($request->medicine_price) : 0;
        $total = array_sum($request->price) + $medicine_price;

        $treatment->dentist_id = Auth::user()->id;
        $treatment->member_id  = $request->member_id;
        $treatment->title      = $request->title;
        $treatment->total      = $total;
        $treatment->image      = $request->film;
        $treatment->save();

        Detail::where('treatment_id', $treatment->id)->delete();

        foreach($request->id as $k => $rs):
            $detail = new Detail;
            $detail->treatment_id = $treatment->id;
            $detail->fee_list_id  = $rs;
            $detail->name         = $request->name[$k];
            $detail->price        = $request->price[$k];
            $detail->type         = "treatment";
            $detail->save();
        endforeach;

        if($request->medicine):
            foreach($request->medicine_id as $k => $rs):
                $detail = new Detail;
                $detail->treatment_id = $treatment->id;
                $detail->fee_list_id  = $rs;
                $detail->name         = $request->medicine_name[$k];
                $detail->price        = $request->medicine_price[$k];
                $detail->type       = "medicine";
                $detail->save();
            endforeach;
        endif;

        return redirect()->route('treatments.index')->with('update','Successfully');
    }
}
