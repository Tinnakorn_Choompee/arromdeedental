<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public function scopeGetDesc($query)
    {
        return $query->orderBy('created_at', 'DESC')->get();
    }

    public function treatment()
    {
        return $this->belongsTo(Treatment::class);
    }

    public function authority()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
