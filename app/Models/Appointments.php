<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Appointments extends Model
{
    public function dentist()
    {
        return $this->belongsTo(User::class, 'dentist_id');
    }

    public function authority()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function member()
    {
        return $this->belongsTo(Member::class);
    }
}
