<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    public function scopeGetDesc($query)
    {
        return $query->orderBy('created_at', 'DESC')->get();
    }

    public function medicine()
    {
        return $this->belongsTo(Medicine::class);
    }

    public function equipment()
    {
        return $this->belongsTo(Equipment::class);
    }

    public function authority()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
