<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Treatment extends Model
{
    public function scopeGetDesc($query)
    {
        return $query->orderBy('created_at', 'DESC')->get();
    }

    public function dentist()
    {
        return $this->belongsTo(User::class, 'dentist_id');
    }

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function details_treatment()
    {
        return $this->hasMany(Detail::class)->where('type', 'treatment');    
    }

    public function details_medicine()
    {
        return $this->hasMany(Detail::class)->where('type', 'medicine');    
    }

    public function payment()
    {
        return $this->hasOne(Payment::class);
    }
}
