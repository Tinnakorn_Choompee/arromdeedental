<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fee extends Model
{
    public function items()
    {
        return $this->hasMany(FeeList::class);
    }
}
