<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = [
        'name', 'gender', 'phone', 'email', 'birthday', 'image', 'address', 'disease', 'allergic'
    ];

    public function scopeGetDesc($query)
    {
        return $query->orderBy('created_at', 'DESC')->get();
    }
}
