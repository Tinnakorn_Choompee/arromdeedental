<?php
use Intervention\Image\ImageManagerStatic as Image;
use File as File;
use Carbon as Carbon;
use App\Models\Payment;
use App\Models\User;
use App\Models\Member;

function uploadImageUser($image)
{
    $image['edit_image'] ? in_array($image['edit_image'], ['user.png', 'docter.png', 'member.png']) ? : File::delete('image/users/'.$image['edit_image']) : NULL;
    $file = $image['image'];
    $name = str_random(5).$file->getClientOriginalName();
    $image_resize = Image::make($file->getRealPath());
    $image_resize->resize(250, 250);
    $image_resize->save('image/users/'.$name);
    return $name;
}

function uploadImageFilm($image)
{
    $image['edit_image'] ? File::delete('image/film/'.$image['edit_image']) : NULL;
    $file = $image['image'];
    $name = str_random(5).$file->getClientOriginalName();
    $image_resize = Image::make($file->getRealPath());
    $image_resize->resize(1500, 1000);
    $image_resize->save('image/film/'.$name);
    return $name;
}

function birthday($date)
{
    $date   = explode("/",$date);
    $year   = $date[2] - 543;
    $month  = $date[1];
    $day    = $date[0];
    return Carbon::create($year, $month, $day);
}

function gender($gender)
{
    switch ($gender) :
        case 'Male':   return "ชาย"; break;
        case 'Female': return "หญิง"; break;
    endswitch;
}

function date_thai($dt, $time = false)
{
    $Month = [1=>"มกราคม", "กุมภาพันธ์ ","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"];

    $y = date("Y", strtotime($dt))+543;
    $m = date("n", strtotime($dt));
    $d = date("j", strtotime($dt));

    $h = date("H",strtotime($dt));
  	$i = date("i",strtotime($dt));
    $s = date("s",strtotime($dt));
    $datetime = $time == TRUE ? $d.' '.$Month[$m].' '.$y.' || '.$h.':'.$i : $d.' '.$Month[$m].' '.$y;
    return $datetime;
}

function d($date)
{
    $y = Carbon::parse($date)->year + 543;
    return Carbon::parse($date)->format("d/m/".$y);
}

function menu()
{
    return [
        'appointments' => ['route' => route('appointments.index')   , 'icon' => 'icon-copy2 f-18',     'role' => 'appointments-list' , 'submenu' => FALSE, 'name' => 'นัดหมาย'],
        'treatments'   => ['route' => route('treatments.index')     , 'icon' => 'fa-stethoscope f-18', 'role' => 'treatments-list'   , 'submenu' => FALSE, 'name' => 'การรักษา'],
        'payments'     => ['route' => route('payments.index')       , 'icon' => 'icon-cash  f-18',     'role' => 'payments-list'     , 'submenu' => FALSE, 'name' => 'ชำระเงิน'],
        // 'equipment'    => ['route' => route('equipment.index')      , 'icon' => 'fa-wrench  f-18',     'role' => 'equipment-list'    , 'submenu' => FALSE, 'name' => 'อุปกรณ์'],

        'equipment'    => ['title' => 'อุปกรณ์',   'submenu' => TRUE , 'icon'=> 'fa-wrench  f-18', 'permission' => ['medicines-list', 'reimbursement_equipment-list'],
            'list' => [
                'equipment'     => ['route'=> route('equipment.index'),               'role' => 'equipment-list' ,               'name'=>'ข้อมูลอุปกรณ์'],
                'reimbursement' => ['route'=> route('reimbursement_equipment.index'), 'role' => 'reimbursement_equipment-list' , 'name'=>'การเบิกอุปกรณ์'],
            ]
        ],

        'medicines'    => ['title' => 'ยา',   'submenu' => TRUE , 'icon'=> 'fa-medkit  f-18', 'permission' => ['medicines-list', 'reimbursement_medicines-list'],
            'list' => [
                'medicines'     => ['route'=> route('medicines.index'),               'role' => 'medicines-list' ,               'name'=>'ข้อมูลยา'],
                'reimbursement' => ['route'=> route('reimbursement_medicines.index'), 'role' => 'reimbursement_medicines-list' , 'name'=>'การเบิกคืนยา'],
            ]
        ],

        'stock'        => ['title' => 'การสต๊อก', 'submenu' => TRUE , 'icon' => 'fa-cubes',  'permission' => ['stock_medicines-list', 'stock_equipment-list'],
            'list' => [
                'stock_medicines'  => ['route'=> route('stock_medicines.index'), 'role' => 'stock_medicines-list' , 'name'=>'ยา'],
                'stock_equipment'  => ['route'=> route('stock_equipment.index'), 'role' => 'stock_equipment-list' , 'name'=>'อุปกรณ์'],
            ]
        ],

        // 'reimbursement' => ['title' => 'การเบิกคืน', 'submenu' => TRUE , 'icon' => 'icon-clipboard4', 'permission' => ['reimbursement_medicines-list', 'reimbursement_equipment-list'],
        //     'list'      => [
        //         'reimbursement_medicines'  => ['route'=> route('reimbursement_medicines.index'), 'role' => 'reimbursement_medicines-list' , 'name'=>'ยา'],
        //         'reimbursement_equipment'  => ['route'=> route('reimbursement_equipment.index'), 'role' => 'reimbursement_equipment-list' , 'name'=>'อุปกรณ์'],
        //     ]
        // ],

        'type'     => ['title' => 'หน่วย',   'submenu' => TRUE , 'icon'=> 'fa-cogs f-18',     'permission' => ['type_medicines-list', 'type_equipment-list'],
            'list' => [
                'type_medicines'  => ['route'=> route('type_medicines.index'), 'role' => 'type_medicines-list' , 'name'=>'ยา'],
                'type_equipment'  => ['route'=> route('type_equipment.index'), 'role' => 'type_equipment-list' , 'name'=>'อุปกรณ์'],
            ]
        ],

        'role' => ['route' => route('role.index')  , 'icon' => 'icon-collaboration  f-18',  'role' => 'role-list' , 'submenu' => FALSE, 'name' => 'กำหนดสิทธิ์ผู้ใช้'],
    ];
}

function user()
{
    return [
        'authority' => ['route' => route('authority.index') , 'icon' => 'icon-users'      , 'role' => 'authority-list' , 'submenu' => FALSE, 'name' => 'เจ้าหน้าที่'],
        'dentist'   => ['route' => route('dentist.index')   , 'icon' => 'fa-user-md f-18' , 'role' => 'dentist-list'   , 'submenu' => FALSE, 'name' => 'ทันตแพทย์'],
        'member'    => ['route' => route('member.index')    , 'icon' => 'icon-users4'     , 'role' => 'member-list'    , 'submenu' => FALSE, 'name' => 'ผู้รับบริการ'],
    ];
}

function payment_count()
{
    return Payment::where('status', 'proceed')->count();
}

function payment_proceed()
{
    return Payment::where('status', 'proceed')->GetDesc();
}

function Numberthai($amount_number)
{
    $amount_number = number_format($amount_number, 2, ".","");
    $pt = strpos($amount_number , ".");
    $number = $fraction = "";
    if ($pt === false)
        $number = $amount_number;
    else
    {
        $number   = substr($amount_number, 0, $pt);
        $fraction = substr($amount_number, $pt + 1);
    }

    $ret = "";
    $baht = ReadNumber($number);
    if ($baht != "")
        $ret .= $baht . "บาท";

    $satang = ReadNumber($fraction);
    if ($satang != "")
        $ret .=  $satang . "สตางค์";
    else
        $ret .= "";
    return $ret;
}

function ReadNumber($number)
{
    $position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
    $number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
    $number = $number + 0;
    $ret = "";
    if ($number == 0) return $ret;
    if ($number > 1000000)
    {
        $ret .= ReadNumber(intval($number / 1000000)) . "ล้าน";
        $number = intval(fmod($number, 1000000));
    }

    $divider = 100000;
    $pos = 0;
    while($number > 0)
    {
        $d = intval($number / $divider);
        $ret .= (($divider == 10) && ($d == 2)) ? "ยี่" :
            ((($divider == 10) && ($d == 1)) ? "" :
            ((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
        $ret .= ($d ? $position_call[$pos] : "");
        $number = $number % $divider;
        $divider = $divider / 10;
        $pos++;
    }
    return $ret;
}

function users_count($user)
{
    switch($user):
        case "dentist"   : $count = User::role('dentist')->count();   break;
        case "authority" : $count = User::role('authority')->count(); break;
        case "member"    : $count = Member::count(); break;
    endswitch;
    return $count;
}

function treatment_month()
{
    for ($i=1; $i <= 12; $i++) :
        $payment[] = Payment::where('status', 'success')->whereMonth('updated_at', $i)->whereYear('updated_at', date('Y'))->count();
    endfor;
    return $payment;
}
?>
