<?php
// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('หน้าหลัก', route('home'));
});

// Profile
Breadcrumbs::register('profile.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');-
    $breadcrumbs->push('โปรไฟล์', route('profile.index'));
});
Breadcrumbs::register('profile.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('profile.index');
    $breadcrumbs->push('แก้ไขโปรไฟล์', route('profile.edit'));
});

// Authority
Breadcrumbs::register('authority.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('เจ้าหน้าที่', route('authority.index'));
});
Breadcrumbs::register('authority.create', function ($breadcrumbs) {
    $breadcrumbs->parent('authority.index');
    $breadcrumbs->push('เพิ่มเจ้าหน้าที่', route('authority.create'));
});
Breadcrumbs::register('authority.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('authority.index');
    $breadcrumbs->push('แก้ไขเจ้าหน้าที่', route('authority.edit', $id));
});
Breadcrumbs::register('authority.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('authority.index');
    $breadcrumbs->push('ข้อมูลเจ้าหน้าที่', route('authority.show', $id));
});

// Dentist
Breadcrumbs::register('dentist.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ทันตแพทย์', route('dentist.index'));
});
Breadcrumbs::register('dentist.create', function ($breadcrumbs) {
    $breadcrumbs->parent('dentist.index');
    $breadcrumbs->push('เพิ่มทันตแพทย์', route('dentist.create'));
});
Breadcrumbs::register('dentist.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('dentist.index');
    $breadcrumbs->push('แก้ไขทันตแพทย์', route('dentist.edit', $id));
});
Breadcrumbs::register('dentist.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('dentist.index');
    $breadcrumbs->push('ข้อมูลทันตแพทย์', route('dentist.show', $id));
});

// Member
Breadcrumbs::register('member.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ผู้รับบริการ', route('member.index'));
});
Breadcrumbs::register('member.create', function ($breadcrumbs) {
    $breadcrumbs->parent('member.index');
    $breadcrumbs->push('เพิ่มผู้รับบริการ', route('member.create'));
});
Breadcrumbs::register('member.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('member.index');
    $breadcrumbs->push('แก้ไขผู้รับบริการ', route('member.edit', $id));
});
Breadcrumbs::register('member.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('member.index');
    $breadcrumbs->push('ข้อมูลผู้รับบริการ', route('member.show', $id));
});

// Midcines
Breadcrumbs::register('medicines.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ยา', route('medicines.index'));
});
Breadcrumbs::register('medicines.create', function ($breadcrumbs) {
    $breadcrumbs->parent('medicines.index');
    $breadcrumbs->push('เพิ่มยา', route('medicines.create'));
});
Breadcrumbs::register('medicines.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('medicines.index');
    $breadcrumbs->push('แก้ไขยา', route('medicines.edit', $id));
});
Breadcrumbs::register('medicines.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('medicines.index');
    $breadcrumbs->push('ข้อมูลยา', route('medicines.show', $id));
});

// Equipment
Breadcrumbs::register('equipment.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('อุปกรณ์', route('equipment.index'));
});
Breadcrumbs::register('equipment.create', function ($breadcrumbs) {
    $breadcrumbs->parent('equipment.index');
    $breadcrumbs->push('เพิ่มอุปกรณ์', route('equipment.create'));
});
Breadcrumbs::register('equipment.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('equipment.index');
    $breadcrumbs->push('แก้ไขอุปกรณ์', route('equipment.edit', $id));
});
Breadcrumbs::register('equipment.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('equipment.index');
    $breadcrumbs->push('ข้อมูลอุปกรณ์', route('equipment.show', $id));
});

// Type_medicines
Breadcrumbs::register('type_medicines.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ประเภทยา', route('type_medicines.index'));
});
Breadcrumbs::register('type_medicines.create', function ($breadcrumbs) {
    $breadcrumbs->parent('type_medicines.index');
    $breadcrumbs->push('เพิ่มประเภทยา', route('type_medicines.create'));
});
Breadcrumbs::register('type_medicines.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('type_medicines.index');
    $breadcrumbs->push('แก้ไขประเภทยา', route('type_medicines.edit', $id));
});
Breadcrumbs::register('type_medicines.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('type_medicines.index');
    $breadcrumbs->push('ข้อมูลประเภทยา', route('type_medicines.show', $id));
});

// Type_Equipment
Breadcrumbs::register('type_equipment.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ประเภทอุปกรณ์', route('type_equipment.index'));
});
Breadcrumbs::register('type_equipment.create', function ($breadcrumbs) {
    $breadcrumbs->parent('type_equipment.index');
    $breadcrumbs->push('เพิ่มประเภทอุปกรณ์', route('type_equipment.create'));
});
Breadcrumbs::register('type_equipment.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('type_equipment.index');
    $breadcrumbs->push('แก้ไขประเภทอุปกรณ์', route('type_equipment.edit', $id));
});
Breadcrumbs::register('type_equipment.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('type_equipment.index');
    $breadcrumbs->push('ข้อมูลประเภทอุปกรณ์', route('type_equipment.show', $id));
});

// Reimbursement_medicines
Breadcrumbs::register('reimbursement_medicines.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('การเบิกคืนยา', route('reimbursement_medicines.index'));
});
Breadcrumbs::register('reimbursement_medicines.create', function ($breadcrumbs) {
    $breadcrumbs->parent('reimbursement_medicines.index');
    $breadcrumbs->push('เพิ่มการเบิกคืนยา', route('reimbursement_medicines.create'));
});
Breadcrumbs::register('reimbursement_medicines.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('reimbursement_medicines.index');
    $breadcrumbs->push('แก้ไขการเบิกคืนยา', route('reimbursement_medicines.edit', $id));
});
Breadcrumbs::register('reimbursement_medicines.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('reimbursement_medicines.index');
    $breadcrumbs->push('ข้อมูลการเบิกคืนยา', route('reimbursement_medicines.show', $id));
});

// Reimbursement_equipment
Breadcrumbs::register('reimbursement_equipment.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('การเบิกอุปกรณ์', route('reimbursement_equipment.index'));
});
Breadcrumbs::register('reimbursement_equipment.create', function ($breadcrumbs) {
    $breadcrumbs->parent('reimbursement_equipment.index');
    $breadcrumbs->push('เพิ่มการเบิกอุปกรณ์', route('reimbursement_equipment.create'));
});
Breadcrumbs::register('reimbursement_equipment.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('reimbursement_equipment.index');
    $breadcrumbs->push('แก้ไขการเบิกอุปกรณ์', route('reimbursement_equipment.edit', $id));
});
Breadcrumbs::register('reimbursement_equipment.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('reimbursement_equipment.index');
    $breadcrumbs->push('ข้อมูลการเบิกอุปกรณ์', route('reimbursement_equipment.show', $id));
});

// Appointments
Breadcrumbs::register('appointments.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('การนัดหมาย', route('appointments.index'));
});
Breadcrumbs::register('appointments.create', function ($breadcrumbs) {
    $breadcrumbs->parent('appointments.index');
    $breadcrumbs->push('เพิ่มการนัดหมาย', route('appointments.create'));
});
Breadcrumbs::register('appointments.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('appointments.index');
    $breadcrumbs->push('แก้ไขการนัดหมาย', route('appointments.edit', $id));
});
Breadcrumbs::register('appointments.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('appointments.index');
    $breadcrumbs->push('ข้อมูลการนัดหมาย', route('appointments.show', $id));
});

// Treatments
Breadcrumbs::register('treatments.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('การรักษา', route('treatments.index'));
});
Breadcrumbs::register('treatments.create', function ($breadcrumbs) {
    $breadcrumbs->parent('treatments.index');
    $breadcrumbs->push('เพิ่มการรักษา', route('treatments.create'));
});
Breadcrumbs::register('treatments.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('treatments.index');
    $breadcrumbs->push('แก้ไขการรักษา', route('treatments.edit', $id));
});
Breadcrumbs::register('treatments.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('treatments.index');
    $breadcrumbs->push('ข้อมูลการรักษา', route('treatments.show', $id));
});

// Payments
Breadcrumbs::register('payments.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ชำระเงิน', route('payments.index'));
});
Breadcrumbs::register('payments.create', function ($breadcrumbs) {
    $breadcrumbs->parent('payments.index');
    $breadcrumbs->push('เพิ่มชำระเงิน', route('payments.create'));
});
Breadcrumbs::register('payments.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('payments.index');
    $breadcrumbs->push('แก้ไขชำระเงิน', route('payments.edit', $id));
});
Breadcrumbs::register('payments.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('payments.index');
    $breadcrumbs->push('ข้อมูลชำระเงิน', route('payments.show', $id));
});

// Stock_medicines
Breadcrumbs::register('stock_medicines.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('การสต๊อกยา', route('stock_medicines.index'));
});
Breadcrumbs::register('stock_medicines.create', function ($breadcrumbs) {
    $breadcrumbs->parent('stock_medicines.index');
    $breadcrumbs->push('เพิ่มการสต๊อกยา', route('stock_medicines.create'));
});

// Stock_equipment
Breadcrumbs::register('stock_equipment.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('การสต๊อกอุปกรณ์', route('stock_equipment.index'));
});
Breadcrumbs::register('stock_equipment.create', function ($breadcrumbs) {
    $breadcrumbs->parent('stock_equipment.index');
    $breadcrumbs->push('เพิ่มการสต๊อกอุปกรณ์', route('stock_equipment.create'));
});

// Role
Breadcrumbs::register('role.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('สิทธิ์การใช้งาน', route('role.index'));
});
Breadcrumbs::register('role.create', function ($breadcrumbs) {
    $breadcrumbs->parent('role.index');
    $breadcrumbs->push('เพิ่มสิทธิ์การใช้งาน', route('role.create'));
});
Breadcrumbs::register('role.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('role.index');
    $breadcrumbs->push('แก้ไขสิทธิ์การใช้งาน', route('role.edit', $id));
});
Breadcrumbs::register('role.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('role.index');
    $breadcrumbs->push('ข้อมูลสิทธิ์การใช้งาน', route('role.show', $id));
});



