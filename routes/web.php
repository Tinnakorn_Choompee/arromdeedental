<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', 'home');

Route::namespace('Auth')->group(function () {
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::post('logout', 'LoginController@logout')->name('logout');
});

Route::get('home', 'HomeController@index')->name('home');

Route::middleware('auth')->group(function () {
    Route::resource('role','RoleController');
    Route::prefix('profile')->group(function () {
        Route::GET('/', 'ProfileController@index')->name('profile.index');
        Route::GET('edit', 'ProfileController@edit')->name('profile.edit');
        Route::PATCH('update', 'ProfileController@update')->name('profile.update');
    });
    Route::resource('authority', 'AuthorityController');
    Route::resource('dentist',   'DentistController');
    Route::resource('member',    'MemberController');
    Route::resource('medicines', 'MedicineController');
    Route::resource('equipment', 'EquipmentController');
    Route::resource('treatments', 'TreatmentController');
    Route::resource('appointments', 'AppointmentsController');

    Route::PATCH('success/appointment', 'AppointmentsController@success');

    Route::GET('treatments/confirm/create', 'TreatmentController@confirm')->name('treatments.confirm');
    Route::POST('treatments/treatments', 'TreatmentController@save')->name('treatments.save');
    Route::GET('treatments/confirm/change', 'TreatmentController@confirm_change')->name('treatments.confirm_change');
    Route::PATCH('treatments/treatments/{treatment}', 'TreatmentController@change')->name('treatments.change');

    Route::resource('payments', 'PaymentController');
    Route::get('payments/print/{payment}', 'PaymentController@print')->name('payments.print');

    Route::resource('type_medicines', 'UnitMedicinesController');
    Route::resource('type_equipment', 'UnitEquipmentController');

    Route::resource('reimbursement_medicines', 'ReimbursementMedicinesController');
    Route::resource('reimbursement_equipment', 'ReimbursementEquipmentController');

    Route::PATCH('takeback/reimbursement_medicines', 'ReimbursementMedicinesController@takeback')->name('reimbursement_medicines.takeback');
    Route::PATCH('takeback/reimbursement_equipment', 'ReimbursementEquipmentController@takeback')->name('reimbursement_equipment.takeback');

    Route::GET('/json-medicines/{medicine}', 'ApiController@medicines');
    Route::GET('/json-equipment/{equipment}', 'ApiController@equipment');

    Route::get('stock_medicines',    'StockMedicineController@index')->name('stock_medicines.index');
    Route::post('stock_medicines',   'StockMedicineController@store')->name('stock_medicines.store');
    Route::delete('stock_medicines', 'StockMedicineController@delete')->name('stock_medicines.delete');

    Route::get('stock_equipment',    'StockEquipmentController@index')->name('stock_equipment.index');
    Route::post('stock_equipment',   'StockEquipmentController@store')->name('stock_equipment.store');
    Route::delete('stock_equipment', 'StockEquipmentController@delete')->name('stock_equipment.delete');
});
