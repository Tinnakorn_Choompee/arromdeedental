/* ------------------------------------------------------------------------------
 *
 *  # Custom JS code
 *
 *  Place here all your custom js. Make sure it's loaded after app.js
 *
 * ---------------------------------------------------------------------------- */
$('.datatable-basic').DataTable({
    columnDefs: [{
        orderable: false,
        targets: [ 3,4,5 ]
    }],
    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"p>',
    language: {
        search: '<span>ค้นหา : </span> _INPUT_',
        lengthMenu: '<span>แสดง : </span> _MENU_',
        paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
    }
});

$('.datatable-member').DataTable({
    columnDefs: [{
        orderable: false,
        targets: [ 3,6,7 ]
    }],
    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"p>',
    language: {
        search: '<span>ค้นหา : </span> _INPUT_',
        lengthMenu: '<span>แสดง : </span> _MENU_',
        paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
    }
});

$('.datatable-type').DataTable({
    columnDefs: [{
        orderable: false,
        targets: [ 2 ]
    }],
    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"p>',
    language: {
        search: '<span>ค้นหา : </span> _INPUT_',
        lengthMenu: '<span>แสดง : </span> _MENU_',
        paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
    }
});

$('.datatable-medicines').DataTable({
    columnDefs: [{
        orderable: false,
        targets: [ 3,6 ]
    }],
    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"p>',
    language: {
        search: '<span>ค้นหา : </span> _INPUT_',
        lengthMenu: '<span>แสดง : </span> _MENU_',
        paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
    }
});

$('.datatable-reimbursement').DataTable({
    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"p>',
    language: {
        search: '<span>ค้นหา : </span> _INPUT_',
        lengthMenu: '<span>แสดง : </span> _MENU_',
        paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
    }
});

$('.datatable-appointments').DataTable({
    columnDefs: [{
        orderable: false,
        targets: [ 7 ]
    }],
    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"p>',
    language: {
        search: '<span>ค้นหา : </span> _INPUT_',
        lengthMenu: '<span>แสดง : </span> _MENU_',
        paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
    }
});

$('.datatable-treatments').DataTable({
    columnDefs: [{
        orderable: false,
        targets: [ 6 ]
    }],
    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"p>',
    language: {
        search: '<span>ค้นหา : </span> _INPUT_',
        lengthMenu: '<span>แสดง : </span> _MENU_',
        paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
    }
});

$('.datatable-stock').DataTable({
    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"p>',
    language: {
        search: '<span>ค้นหา : </span> _INPUT_',
        lengthMenu: '<span>แสดง : </span> _MENU_',
        paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
    }
});


// Defaults
var setCustomDefaults = function () {
    swal.setDefaults({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });
}

setCustomDefaults();

// Initialize module
// ------------------------------

$('.select-search').select2();

$('.form-control-uniform').uniform();

// Custom select
$('.form-control-uniform-custom').uniform({
    fileButtonClass: 'action btn bg-blue',
});

$('.form-check-input-styled-danger').uniform({
    wrapperClass: 'border-danger-600 text-danger-800'
});

// Full featured example
$('.multiselect-full-featured').multiselect({
    enableFiltering: true,
    maxHeight: 450,
    nonSelectedText : "-- เลือกการรักษา --"
});

$('.multiselect-full-medicines').multiselect({
    maxHeight: 450,
    nonSelectedText : "-- เลือกรายการยา --"
});

// Js Project
$('.btn-del').on('click',function(){
    const id = $(this).data('id');
    swal({
        title: 'Are you sure?',
        text: "ต้องการที่จะลบ ข้อมูล นี้ใช่หรือไม่ !!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'ลบ',
        cancelButtonText: 'ยกเลิก',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false,

    }).then(function (result) {
        if (result.value) {
            $( "#delete-"+id ).submit();
        }
    });
});

$('#toggle-all').change(function() {
    $(this).prop('checked') ? $('.check-all').prop('checked',true) : $('.check-all').prop('checked',false)
    $(this).prop('checked') ? $('.btn-all').prop('disabled',false) : $('.btn-all').prop('disabled',true)
    $.uniform.update('.check-all');
});

$('.check-all').click(function() {
    $(".check-all:checked").length == 0 ? $('.btn-all').prop('disabled',true) : $('.btn-all').prop('disabled',false)
});

$('.form-check-input-styled-danger').uniform({
    wrapperClass: 'border-danger-600 text-danger-800'
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('.btn-stock').on('click',function(){
    var id   = $(this).data('id');
    var name = $(this).data('name');
    var type = $(this).data('type');
    swal({
        html: '<b> สต๊อก ' + name+ "</b>",
        input: 'number',
        inputClass: 'form-control',
        inputPlaceholder: 'กรุณาระบุตัวเลข มากกว่า 0',
        // background: '#fff url(../../image/seamless.png) repeat'
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: "/stock_"+type,
                data: {id : id , qty : result.value},
                success: function(rs){
                    if(rs == "success")
                    {
                        swal({ title: 'Success!', text: 'ทำการสต๊อกเรียบร้อยแล้ว', type: 'success', showConfirmButton: false, showCloseButton: false});
                        setTimeout(function() {
                            location.reload();
                        }, 500);
                    }
                }
            });
        }
    });
});

function getAge(d1, d2){

    d2 = d2 || new Date();
    console.log(d1);
    console.log(d2);
    var diff = d2.getTime() - d1.getTime();
    var age  = Math.floor(diff / (1000 * 60 * 60 * 24 * 365.25));
    return isNaN(diff) ? "" : (age < 0 ? 0 : age);
}

$("#birthday").on('change', function() {
    var arr_date = $(this).val().split("/");
    $("#age").val(getAge(new Date(arr_date[2] - 543, arr_date[1], arr_date[0])));
});

$(".phone").on("keypress keyup blur",function (event) {
    $(this).val($(this).val().replace(/[^\d].+/, ""));
    if ((event.which < 48 || event.which > 57)) { event.preventDefault() }
});

function readImage(input) {
    if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
        $('#photo').attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
    }
}

$("#ActiveImage").change(function() {
    readImage(this);
});

